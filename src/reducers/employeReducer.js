import * as employ from "../constants/account";

const initialState = {
  listEmploye: [],
  createEmploye: false,
  yourInforEmp: {},
  deleteEmploy: "",
  editEmploy: false,
  onEdit: "",
  dialogFull: {},
  dialogFullMail: {}
};
export default function (state = initialState, action) {
  switch (action.type) {
    case employ.CREATE_USER:
      return {
        ...state,
        createEmploye: true
      };

    case employ.RESET_DATA:
      return {
        ...state,
        createEmploye: false
      };

    case employ.GETLIST_SUCCESS:
      return {
        ...state,
        listEmploye: action.payload
      };
    case employ.ON_EDIT_EMPLOYE:
      return {
        ...state,
        onEdit: "onEdit"
      };
    case employ.GET_YOUR_EMPLOY:
      return {
        ...state,
        yourInforEmp: action.payload
      };

    case employ.DEL_EMPLOYE:
      return {
        ...state,
        deleteEmploy: action.payload
      };
    case employ.EDIT_EMPLOYE:
      return {
        ...state,
        editEmploy: true
      };

    case employ.RESET_DEL_EMPLOYE:
      return {
        ...state,
        createEmploye: false,
        yourInforEmp: {},
        deleteEmploy: "",
        editEmploy: false,
        onEdit: ""
      };

    case employ.SHOW_FULL_DIALOG:
      return {
        ...state,
        dialogFull: action.payload
      };
    case employ.SHOW_FULL_DIALOG_MAIL:
      return {
        ...state,
        dialogFullMail: action.payload
      };
    case employ.RESET_DIALOG:
      return {
        ...state,
        dialogFull: {},
        dialogFullMail: {}
      };
    default:
      return state;
  }
}

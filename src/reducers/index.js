import { combineReducers } from "redux";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";
import spinnerReducer from "./spinnerReducer";
import employeReducer from "./employeReducer";
import departReducer from "./departReducer";
import accountReducer from "./accountReducer";
import roleReducer from "./roleReducer";
import positionReducer from "./positionReducer";
import contractReducer from "./contractReducer";
import benefitReducer from "./benefitReducer";
import leavesReducers from "./leavesReducers";
import backupReducer from "./backupReducer";

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  loading: spinnerReducer,
  employes: employeReducer,
  departs: departReducer,
  accounts: accountReducer,
  roles: roleReducer,
  positions: positionReducer,
  cons: contractReducer,
  benefits: benefitReducer,
  leaves: leavesReducers,
  backups: backupReducer
});
import * as accounts from "../constants/account";

const initialState = {
  listAccUnset: [],
  listAcc: [],
  deleteuser: "",
  editAccount: "",
  listRoleFuc: [],
  session: "",
  changePassSuccess: false,
  resetPassAdmin: "",
  resetPassUser: "",
  verifyUser: ""
};

export default function (state = initialState, action) {
  switch (action.type) {
    case accounts.SET_SESSION:
      return {
        ...state,
        session: "true"
      };

    case accounts.VERIFY_USER:
      return {
        ...state,
        verifyUser: action.payload
      };

    case accounts.RESET_PASS_USER:
      return {
        ...state,
        resetPassUser: action.payload
      };

    case accounts.RESET_PASS_ADMIN:
      return {
        ...state,
        resetPassAdmin: action.payload
      };

    case accounts.GETLIST_UNSET:
      return {
        ...state,
        listAccUnset: action.payload
      };

    case accounts.CHANGE_PASS:
      return {
        ...state,
        changePassSuccess: action.payload
      };

    case accounts.GETLIST_ACC:
      return {
        ...state,
        listAcc: action.payload
      };

    case accounts.DELETE_ACC:
      return {
        ...state,
        deleteuser: action.payload
      };

    case accounts.RESET_ACCOUNTS:
      return {
        ...state,
        deleteuser: "",
        editAccount: "",
        session: "",
        resetPassAdmin: "",
        resetPassUser: "",
        verifyUser: "",
        changePassSuccess: false
      };

    case accounts.GET_LIST_ROLE_FUNC:
      return {
        ...state,
        listRoleFuc: action.payload
      };

    case accounts.EDIT_ACCOUNT:
      return {
        ...state,
        editAccount: action.payload
      };

    default:
      return state;
  }
}

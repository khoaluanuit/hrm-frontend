import * as contract from "../constants/contract";
const initialState = {
  listCons: [],
  deleteCons: "",
  editCons: {},
  addCons: false
};
export default function(state = initialState, action) {
  switch (action.type) {
    case contract.GET_LIST_CONTRACT:
      return {
        ...state,
        listCons: action.payload
      };
    case contract.ADD_CONTRACT:
      return {
        ...state,
        addCons: true
      };
    case contract.DELETE_CONTRACT:
      return {
        ...state,
        deleteCons: action.payload
      };
    case contract.EDIT_CONTRACT:
      return {
        ...state,
        editCons: action.payload
      };
    case contract.RESET_CONTRACT:
      return {
        ...state,
        deleteCons: "",
        editCons: "",
        addCons: false
      };
    default:
      return state;
  }
}

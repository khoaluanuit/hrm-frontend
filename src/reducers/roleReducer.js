import * as role from "../constants/role";
const initialState = {
    listRoles: []
  };;
export default function (state = initialState, action) {
  switch (action.type) {
    case role.GET_LIST_ROLE:
      return {
          ...state,
          listRoles: action.payload,
      };

    default:
      return state;
  }
}
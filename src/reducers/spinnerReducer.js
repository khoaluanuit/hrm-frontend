import * as spin from "../constants/spinner";
const initialState = false;
export default function (state = initialState, action) {
  switch (action.type) {
    case spin.DATA_LOADING:
      return true;

    case spin.DATA_DONE:
      return false; 

    default:
      return state;
  }
}
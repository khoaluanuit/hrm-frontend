import { GET_ERRORS, RESET_ERRORS } from "../constants/auth";
const initialState = {};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ERRORS: {

      if (action.payload.Message === 'Vui lòng tiến hành đăng nhập lại!') {
        localStorage.removeItem("x-auth-token");
        localStorage.removeItem("role");
        localStorage.removeItem("account");
        setTimeout(() => {
          window.location.reload()
        }, 2000);
      }
      return action.payload;
    }

    case RESET_ERRORS:
      return initialState;

    default:
      return state;
  }
}
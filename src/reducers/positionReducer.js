import * as pos from "../constants/position";
const initialState = {
  listPos: [],
  listPosFilter: [],
  deletePos: "",
  editPos: {},
  addPos: false
};
export default function(state = initialState, action) {
  switch (action.type) {
    case pos.GET_LIST_POS:
      return {
        ...state,
        listPos: action.payload
      };
    case pos.GET_LIST_POSFIL:
      return {
        ...state,
        listPosFilter: action.payload
      };
    case pos.ADD_POSITION:
      return {
        ...state,
        addPos: true
      };
    case pos.DELETE_POSITION:
      return {
        ...state,
        deletePos: action.payload
      };
    case pos.EDIT_POSITION:
      return {
        ...state,
        editPos: action.payload
      };
    case pos.RESET_POSITION:
      return {
        ...state,
        listPos: [],
        listPosFilter: [],
        deletePos: "",
        editPos: "",
        addPos: false
      };
    default:
      return state;
  }
}

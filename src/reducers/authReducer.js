import {
  SET_CURRENT_USER,
  GET_USERS,
  GET_USER,
  RESET_DATA,
  REGISTER_SUCCESS,
  RESET_REGISTER,
  LOGIN_SUCCESS
} from "../constants/auth";

const isEmpty = require("is-empty");
const initialState = {
  isAuthenticated: false,
  isRegisSuccess: false,
  user: {},
  users: [],
  userOthers: [],
  loginSuccess: {}
};
export default function(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        loginSuccess: action.payload
      };
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
        isRegisSuccess: true
      };
    case RESET_REGISTER:
      return {
        ...state,
        isRegisSuccess: false,
        loginSuccess: {}
      };
    case GET_USERS:
      return {
        ...state,
        users: action.payload
      };
    case GET_USER:
      return {
        ...state,
        userOthers: action.payload
      };
    case RESET_DATA:
      return {
        ...state,
        userOthers: []
      };

    default:
      return state;
  }
}

import * as backup from "../constants/backup";
const initialState = {
    listBacks: [],
    deleteBack: "",
    createNewBack: "",
    restoreBack: "",
};
export default function (state = initialState, action) {
    switch (action.type) {
        case backup.GET_LIST_BACKUP:
            return {
                ...state,
                listBacks: action.payload
            };

        case backup.RESTORE_BACKUP:
            return {
                ...state,
                restoreBack: action.payload
            };

        case backup.CREATE_BACKUP:
            return {
                ...state,
                createNewBack: action.payload
            };

        case backup.DELETE_BACKUP:
            return {
                ...state,
                deleteBack: action.payload
            };

        case backup.RESET_BACKUP:
            return {
                ...state,
                listBacks: [],
                deleteBack: "",
                createNewBack: "",
                restoreBack: ""
            };

        default:
            return state;
    }
}

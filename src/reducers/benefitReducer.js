import * as benefit from "../constants/benefit";
const initialState = {
  listMonths: [],
  yourBens: {},
  deleteBens: "",
  createNewBen: "",
  createNewAll: "",
  editBens: {},
  addBens: false
};
export default function (state = initialState, action) {
  switch (action.type) {
    case benefit.GET_LIST_MONTHS:
      return {
        ...state,
        listMonths: action.payload
      };
    case benefit.CREATE_NEW_TIME:
      return {
        ...state,
        createNewBen: action.payload
      };
    case benefit.CREATE_NEW_ALL:
      return {
        ...state,
        createNewAll: action.payload
      };
    case benefit.RESET_BENEFIT:
      return {
        ...state,
        listMonths: [],
        yourBens: {},
        deleteBens: "",
        editBens: {},
        addBens: false,
        createNewAll: "",
        createNewBen: ""
      };
    case benefit.RESET_YOUR_BEN:
      return {
        ...state,
        yourBens: {}
      };
    case benefit.GET_YOUR_BEN:
      return {
        ...state,
        yourBens: action.payload
      };
    default:
      return state;
  }
}

import * as depart from "../constants/department";
const initialState = {
    list: [],
    createDepart: {},
    deleteDepart: "",
};
export default function (state = initialState, action) {
    switch (action.type) {
        case depart.GET_LIST: {
            return {
                ...state,
                list: action.payload
            };
        }

        case depart.ADD_DEPART: {
            return {
                ...state,
                createDepart: action.payload
            };
        }

        case depart.DELETE_DEPART: {
            return {
                ...state,
                deleteDepart: action.payload
            };
        }
        case depart.RESET_DELETE: {
            return {
                ...state,
                deleteDepart: ""
            };
        }

        default:
            return state;
    }
}
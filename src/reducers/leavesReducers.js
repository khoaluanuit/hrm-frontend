import * as leave from "../constants/leave";
const initialState = {
  leavesAll: [],
  leavesNoPermit: [],
  leavesNotApproveYet: [],
  leavesAllYour: [],
  createSuccess: false,
  deleteSuccess: "",
  editLeaveRec: "",
};
export default function (state = initialState, action) {
  switch (action.type) {
    case leave.GET_ALL_LEAVENOTYET:
      return {
        ...state,
        leavesNotApproveYet: action.payload
      };
    case leave.DELETE_LEAVE:
      return {
        ...state,
        deleteSuccess: action.payload
      };
    case leave.GET_YOUR_LEAVE:
      return {
        ...state,
        leavesAllYour: action.payload
      };
    case leave.CREATE_LEAVE:
      return {
        ...state,
        createSuccess: true
      };
      case leave.EDIT_LEAVE:
      return {
        ...state,
        editLeaveRec: action.payload
      };
    case leave.RESET_LEAVE:
      return {
        ...state,
        leavesAll: [],
        leavesNoPermit: [],
        leavesNotApproveYet: [],
        leavesAllYour: [],
        createSuccess: false,
        deleteSuccess: "",
        editLeaveRec: ""
      };
    default:
      return state;
  }
}

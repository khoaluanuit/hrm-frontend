import React from "react";
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function ExportExel({ ...props }) {
  const data = props.data;
  const fileName = props.fileName;
  const btnName = props.btnName || "Export Exel";

  const generateCol = () => {
    if (data.columns.length) {
      return data.columns.map((item, index) => {
        return (
          <ExcelColumn key={index} label={item.title} value={item.field} />
        );
      });
    }
  };

  return (
    <ExcelFile element={<button>{btnName}</button>} filename={fileName}>
      <ExcelSheet data={data.data} name={fileName}>
        {generateCol()}
      </ExcelSheet>
    </ExcelFile>
  );
}

import axios from 'axios';

class AxiosService {
  constructor() {
    this.config = {
      headers: {
        'x-auth-token': localStorage['x-auth-token']
      }
    }
    const instance = axios.create();
    this.instance = instance;
    instance.interceptors.response.use(this.handleSuccess, this.handleError);
  }

  handleSuccess(response) {
    return response;
  }

  handleError(error) {
    return Promise.reject(error);
  }

  setToken(token){
    this.config = {
      headers: {
        'x-auth-token': token
      }
    }
  }

  get(url) {
    return this.instance.get(url, this.config);
  }

  post(url, body) {
    // console.log(this.config);
    return this.instance.post(url, body, this.config);
  }

  put(url, body) {
    return this.instance.put(url, body, this.config);
  }

  delete(url, body) {
    return this.instance.delete(url, this.config );
  }
}

export default new AxiosService();
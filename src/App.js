import "bootstrap/dist/js/bootstrap.bundle";
import jwt_decode from "jwt-decode";
import React, { Component } from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { logoutUser, setCurrentUser } from "./actions/authActions";
import PrivateRoute from "./components/private-route/PrivateRoute";
import Spinner from "./components/Spinner/Spinner";
import Admin from "./layout/Admin";
import Login from "./layout/Login.js";
import store from "./store";
import "./assets/css/animate.css";

if (localStorage["x-auth-token"]) {
  // Set auth token header auth
  const token = localStorage["x-auth-token"];
  const decoded = jwt_decode(token);
  store.dispatch(setCurrentUser(decoded));
  const currentTime = Date.now() / 1000;
  // console.log(decoded)

  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());
    // Redirect to login
    window.location.href = "./login";
  }
}

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Provider store={store}>
          <Spinner />
          <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnVisibilityChange
            draggable
            pauseOnHover
          >

          </ToastContainer>
          <Router>
            <Switch>
              <Route path="/" component={Login} exact />
              <PrivateRoute path="/admin" component={Admin}></PrivateRoute>
              <PrivateRoute exact path="/*" component={Admin} />
            </Switch>
          </Router>
        </Provider>
      </React.Fragment>
    );
  }
}
export default App;

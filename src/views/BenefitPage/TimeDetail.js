import viLocale from "@fullcalendar/core/locales/vi";
import "@fullcalendar/core/main.css";
import dayGridPlugin from "@fullcalendar/daygrid";
import "@fullcalendar/daygrid/main.css";
import interactionPlugin from '@fullcalendar/interaction';
import FullCalendar from "@fullcalendar/react";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import ReactTooltip from "react-tooltip";
const useStyles = makeStyles(theme => ({
  block: {
    // maxWidth: 1000
  }
}));

export default function TimeDetail(rest) {
  // console.log(rest.data.days); 
  const events = rest.data.days.map((item, index) => {
    let tempData = {};
    let month = rest.data.month.length > 1 ? rest.data.month : `0${rest.data.month}`
    tempData.date =
    item.date < 10
      ? `${rest.data.year}-${month}-0${item.date}`
      : `${rest.data.year}-${month}-${item.date}`;

    // Binh thuong
    if (item.checkOut !== "None" && item.checkIn !== "None" && item.lateEarly === 0) {
      tempData.date =null
    }
    if (item.checkOut === "None" && item.checkIn === "None" && item.lateEarly === 0 && item.leave.note === "None") {
      tempData.date =null
    }

    // Chua cap nhat
    if (item.checkOut === "None" && item.checkIn === "None" && (item.leave.reason !== "None" || item.leave.note !== "None")){
      tempData.title = item.leave.reason === 'None' ? "" : item.leave.reason;
      if (item.leave.permit === true){
        tempData.color = '#00d3ee'
      }
      if (item.leave.permit === false){
        tempData.color = '#f55a4e'
      }
    }

    // Di tre
    if (item.lateEarly < 0) {
      tempData.title = ` Giờ vào: ${item.checkIn}
      Giờ ra: ${item.checkOut}
      `;
      tempData.color = '#ffa21a'
    }
    // Di som
    if (item.lateEarly > 0) {
      tempData.title = ` Giờ vào: ${item.checkIn}
      Giờ ra: ${item.checkOut}
      `;
      tempData.color = "#5cb860";
    }
    return tempData;
  });
  const classes = useStyles();
  const handleEventPositioned = info => {
    info.el.setAttribute("data-tip", info.el.innerText);
    ReactTooltip.rebuild();
  };
  return (
    <div>
      <ReactTooltip />
      <FullCalendar
        defaultDate={new Date(rest.data.year, rest.data.month - 1, 20)}
        className={classes.block}
        defaultView="dayGridMonth"
        plugins={[dayGridPlugin, interactionPlugin ]}
        weekends={true}
        locale={viLocale}
        height="parent"
        events={events}
        eventPositioned={e => handleEventPositioned(e)}
        header={{
          left: "title",
          center: "none",
          right: "none"
        }}
      />
    </div>
  );
}

/* eslint-disable react-hooks/exhaustive-deps */
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { makeStyles } from "@material-ui/core/styles";
import { createAllEmp, getKeepIdEmp, getListMonths, getYourTimeKeep, resetBens, resetYourBens } from "actions/benefitActions";
import { getList } from "actions/employeActions";
import { hide, show } from "actions/spinerActions";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from "components/Table/Table.js";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import SelectCom from "react-select";
import Swal from "sweetalert2";
import TimeDetail from "./TimeDetail";

const useStyles = makeStyles(theme => ({
  title: {
    fontSize: "1.25rem"
  },
  submit: {
    margin: '24px 10px 24px'
  },
  icon: {
    display: 'inline-block',
    width: '40%',
    float: 'right',
    height: 15,
    marginLeft: 15,
  },
  guildline: {
    padding: 15,
    boxSizing: "border-box"
  },
  formControl: {
    width: 120,
    marginLeft: 15
  },
  blockControl: {
    display: 'flex',
    alignItems: 'flex-end',
    marginBottom: 20
  },
  formControlSearch: {
    width: 350,
    display: 'inline-block',
    zIndex: 10,
  },
  blockIconSearch: {
    position: "absolute",
    right: "0px",
    top: "10px",
    zIndex: 2,
    backgroundColor: "transparent",
    boxShadow: "none",
    cursor: "pointer"
  },
  iconSearch: {
    fontSize: "18px",
    color: "gray"
  }
}));

export default function BenefitPage() {
  const isEmpty = require("is-empty");
  const listMonths = useSelector(state => state.benefits.listMonths);
  const listEmployes = useSelector(state => state.employes.listEmploye);
  const dispatch = useDispatch();
  const [month, setMonth] = useState("");
  const [employe, setEmploye] = useState(null);
  const [inputSearch, setInputSearch] = useState("");
  const checkRole = () => {
    return ["suAd", "nvNs", "adNs"].includes(localStorage.getItem("role"));
  };
  const classes = useStyles();
  useEffect(() => {
    // dispatch(show());
    if (checkRole()) {
      dispatch(getList());
      dispatch(getListMonths());
    } else {
      dispatch(getListMonths());
    }

    return () => {
      dispatch(resetBens());
      dispatch(hide());
    };
  }, []);
  useEffect(() => {
    if (!isEmpty(listMonths)) {
      const month = new Date().getMonth();
      const year = new Date().getFullYear();
      let flag = false;
      listMonths.forEach((item)=>{
        if (`${month + 1}/${year}` === item) {
          flag = true;
          setMonth(item);
          dispatch(
            getYourTimeKeep({
              month: (month + 1).toString(),
              year: year.toString()
            })
          );
        }
      })
      if (!flag){
        Swal.fire({
          icon: "warning",
          title: "Lỗi dữ liệu",
          text: "Bảng chấm công tháng này chưa được cập nhật"
        });
      }
      // if (`0${month + 1}/${year}` === listMonths[0]) {
      //   setMonth(listMonths[0]);
      //   dispatch(
      //     getYourTimeKeep({
      //       month: (month + 1).toString(),
      //       year: year.toString()
      //     })
      //   );
      // } else {
      //   Swal.fire({
      //     icon: "warning",
      //     title: "Lỗi dữ liệu",
      //     text: "Bảng chấm công tháng này chưa được cập nhật"
      //   });
      // }
    }
  }, [listMonths]);
  const resTimeKeep = useSelector(state => state.benefits.yourBens);
  useEffect(() => {
    console.log(resTimeKeep);
    if (!isEmpty(resTimeKeep.idEmp)) {
      setEmploye(resTimeKeep);
      dispatch(hide());
    } else if (!isEmpty(resTimeKeep.message)) {
      Swal.fire({
        icon: 'error',
        title: resTimeKeep.message,

      })
    }
  }, [resTimeKeep]);

  const rendertListMonths = () => {
    return listMonths.map((item, id) => {
      return (
        <MenuItem key={id} value={item}>
          {item}
        </MenuItem>
      );
    });
  };
  const createNewAll = useSelector(state => state.benefits.createNewAll);
  useEffect(() => {
    if (!isEmpty(createNewAll)) {
      dispatch(hide())
      Swal.fire({
        icon: "success",
        title: "Thành công",
        text: createNewAll.Message,
      });
    }
  }, [createNewAll]);
  const onSubmit = () => {
    Swal.fire({
      title: 'Đồng ý tạo phép?',
      text: `Tính năng cho phép người quản trị khởi tạo trước phép cho tất cả nhân viên trong tháng mới`,
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Đồng ý!',
      cancelButtonText: 'Hủy'
    }).then((result) => {
      dispatch(show())
      if (result.value) {
        dispatch(createAllEmp())

      }
    })
  }
  useEffect(() => {
    if (!isEmpty(listEmployes)) {
      listEmployes.forEach(element => {
        element.value = element.idEmp;
        element.label = `${element.fullname} - ${element.idEmp}`;
      });
    }
  }, [listEmployes]);

  return (
    <GridContainer>
      <GridItem xs={12} sm={7} md={8}>
        <div className={classes.blockfControl}>
          {
            // !isEmpty(listMonths) && checkRole() && employe && <div><SelectCom
            !isEmpty(listMonths) && checkRole()  && <div><SelectCom
              className={classes.formControlSearch}
              options={listEmployes}
              defaultValue={listEmployes.filter(item => {
                return item.idEmp === resTimeKeep.idEmp
              })}
              menuIsOpen={inputSearch !== ""}
              isClearable={true}
              maxMenuHeight={200}
              onChange={(e) => {
                setMonth("");
                !isEmpty(e) && setEmploye(e)
              }}
              onInputChange={(e) => { setInputSearch(e) }}
            /><Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={onSubmit}
            >
                Tạo bảng chấm công
        </Button></div>

          }

          <FormControl className={classes.formControl}>
            <InputLabel>Chọn tháng</InputLabel>
            <Select
              value={month}
              onChange={e => {
                dispatch(resetYourBens());
                console.log(employe.idEmp);
                dispatch(getKeepIdEmp({
                  month: e.target.value.split("/")[0],
                  year: e.target.value.split("/")[1],
                  idEmp: employe.idEmp
                }))
                setMonth(e.target.value);
              }}
            >
              {listMonths.length > 0 && rendertListMonths()}
            </Select>
          </FormControl>

        </div>
        {resTimeKeep.days && <TimeDetail data={resTimeKeep} />}
      </GridItem>

      <GridItem xs={12} sm={5} md={4}>
        <Card className={classes.guildline}>
          <div>
            Nghỉ có phép{" "}
            <span
              className={classes.icon}
              style={{ backgroundColor: "#00d3ee" }}
            ></span>
          </div>
          <div>
            Nghỉ không phép{" "}
            <span
              className={classes.icon}
              style={{ backgroundColor: "#f55a4e" }}
            ></span>
          </div>
          <div>
            Làm thêm giờ
            <span
              className={classes.icon}
              style={{ backgroundColor: "#5cb860" }}
            ></span>
          </div>
          <div>
            Đi trễ
            <span
              className={classes.icon}
              style={{ backgroundColor: "#ffa21a" }}
            ></span>
          </div>
          {/* <div>Chưa cập nhật<span className={classes.icon} style={{ backgroundColor: '#9ea19f' }}></span></div> */}
        </Card>
        {month !== "" && (
          <Card style={{ transition: "0.5s all ease" }}>
            <CardHeader color="info">
              <div className={classes.title}>Thông tin ngày phép</div>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="primary"
                tableHead={["Loại phép", "Số ngày"]}
                tableData={[
                  ["Nghỉ có phép", "1"],
                  ["Nghỉ không phép", "0"],
                  ["Nghỉ bệnh", "1"],
                  ["Nghỉ thai sản", "0"],
                  ["Nghỉ lễ", "2"],
                  ["Tăng ca vào ngày nghỉ", "1"],
                  ["Tăng ca vào ngày lễ", "1"],
                  ["Ca đêm vào ngày nghỉ", "1"],
                  ["Ca đêm ngày lễ", "1"]
                ]}
              />
            </CardBody>
          </Card>
        )}
      </GridItem>
    </GridContainer>
  );
}

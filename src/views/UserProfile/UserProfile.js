/* eslint-disable react-hooks/exhaustive-deps */
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import { resetError } from "actions/authActions";
import { getInfor } from "actions/employeActions";
import { hide } from "actions/spinerActions";
import avatar from "assets/img/faces/avatar.png";
import Card from "components/Card/Card.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import EditEmploye from "components/Employe/EditEmploye";
import GridContainer from "components/Grid/GridContainer.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import "date-fns";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import * as toast from "utils/toasHelper";

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: "27px 0 0 0",
    paddingBottom: "10px",
    minWidth: 120
  },
  cardCategory: {
    textTransform: "uppercase"
  },
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  },
  description: {
    color: "#972FB0",
    fontWeight: "400"
  }
}));

export default function UserProfile() {
  //Get your infor
  useEffect(() => {
    dispatch(getInfor());
  }, []);
  const classes = useStyles();
  const yourInfor = useSelector(state => state.employes.yourInforEmp);

  const dispatch = useDispatch();
  const hasErr = useSelector(state => state.errors);
  const isCreateSuccess = useSelector(state => state.employes.createAccount);
  if (Object.keys(hasErr).length !== 0 && hasErr.constructor === Object) {
    Swal.fire({
      icon: "error",
      title: Object.keys(hasErr)[0],
      text: Object.values(hasErr)[0]
    });
    dispatch(resetError());
    dispatch(hide());
  }
  useEffect(() => {
    if (isCreateSuccess) {
      toast.toastSuccess("Tạo account thành công");
      dispatch(hide());
    }
  }, [isCreateSuccess]);

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12} md={8}>
          <EditEmploye employes={yourInfor || {}} />
        </GridItem>
        <GridItem xs={12} sm={12} md={4}>
            <Card profile>
              <CardAvatar profile>
                <a href="#pablo" onClick={e => e.preventDefault()}>
                  <img src={avatar} alt="..." />
                </a>
              </CardAvatar>
              <CardBody profile>
                <div className={classes.cardCategory}>{yourInfor.position}</div>
                <div className={classes.cardCategory}>
                  {yourInfor.department}
                </div>
                <h4 className={classes.cardTitle}>{yourInfor.fullname}</h4>
                <h2 className={classes.description}>{yourInfor.idEmp}</h2>
              </CardBody>
            </Card>
          </GridItem>
      </GridContainer>
    </div>
  );
}

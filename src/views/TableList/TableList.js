
import React from "react";
import ExportExel from "utils/ExportExel";

export default function TableList() {

  const state = {
    columns: [
      { title: "Tên", field: "name" },
      { title: "Lương", field: "amount" },
      { title: "Giới tính", field: "sex" }
    ],
    data: [
      {
        name: "Johson",
        amount: 30000,
        sex: "M"
      },
      {
        name: "Monika",
        amount: 355000,
        sex: "F"
      },
      {
        name: "John",
        amount: 250000,
        sex: "M"
      },
      {
        name: "Josef",
        amount: 450500,
        sex: "M"
      }
    ]
  };
  return <ExportExel data={state} btnName="btnName" fileName="fileName"/>;
}

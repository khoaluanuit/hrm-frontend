/* eslint-disable react-hooks/exhaustive-deps */
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { getListBackups, resetBackup, createNewBackup, delBackup, restoreBackup } from "actions/backupActions";
import Card from "components/Card/Card.js";
import GridContainer from "components/Grid/GridContainer.js";
import BackupIcon from '@material-ui/icons/Backup';
import GridItem from "components/Grid/GridItem.js";
import MaterialTable from "material-table";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as toast from "utils/toasHelper";
import Swal from "sweetalert2";
import SettingsBackupRestoreIcon from '@material-ui/icons/SettingsBackupRestore';
const useStyles = makeStyles(theme => ({
  card: {
    marginTop: "0 !important"
  },
  root: {
    height: "100vh"
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export default function BackupPage() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const isEmpty = require("is-empty");
  const listBacks = useSelector(state => state.backups.listBacks);
  const createNewBack = useSelector(state => state.backups.createNewBack);
  const deleteBack = useSelector(state => state.backups.deleteBack);
  const restoreBack = useSelector(state => state.backups.restoreBack);

  const [state, setState] = useState({
    columns: [
      { title: "Tên", field: "name" },
      { title: "Ngày tạo", field: "dateCreate", emptyValue: "Chưa có thông tin" },
    ],
    data: []
  });

  useEffect(() => {
    dispatch(getListBackups());
    return () => {
      dispatch(resetBackup());
    }
  }, []);

  useEffect(() => {
    if (!isEmpty(createNewBack)) {
      dispatch(getListBackups());
      toast.toastSuccess(createNewBack);
    }
  }, [createNewBack]);

  useEffect(() => {
    if (!isEmpty(restoreBack)) {
      toast.toastSuccess(restoreBack);
    }
  }, [restoreBack]);

  useEffect(() => {
    if (!isEmpty(deleteBack)) {
      // dispatch(getListBackups());
      toast.toastSuccess(deleteBack);
    }
  }, [deleteBack]);

  useEffect(() => {
    listBacks.forEach(item => {
      item.dateCreate = new Date(item.dateCreate).toLocaleDateString("vi-VN");
    });
    if (!isEmpty(listBacks)) {
      console.log(listBacks);
      setState({ ...state, data: listBacks });
    }
  }, [listBacks]);

  const onSubmit = () => {
    dispatch(createNewBackup());
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={8}>
        <MaterialTable
          title="Danh sách các bản sao lưu"
          columns={state.columns}
          data={state.data}
          editable={{
            onRowDelete: oldData =>
              new Promise(resolve => {
                resolve();
                setState(prevState => {
                  dispatch(delBackup(oldData._id));
                  const data = [...prevState.data];
                  data.splice(data.indexOf(oldData), 1);
                  return { ...prevState, data };
                });
              })
          }}
          actions={[{
            icon: () => <SettingsBackupRestoreIcon color="primary" />,
            tooltip: "Phục hồi dữ liệu",
            onClick: (event, rowData) => {
              Swal.fire({
                title: 'Bạn có chắc?',
                text: `Phục hồi dữ liệu của bản sao lưu ${rowData.name}`,
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Đồng ý!',
                cancelButtonText: 'Hủy'
              }).then((result) => {
                if (result.value) {
                  dispatch(restoreBackup(rowData._id))
                }
              })
            }

          }]
          }
        />
      </GridItem>
      <GridItem xs={12} sm={12} md={4}>
        <Card className={classes.card}>
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <BackupIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sao lưu dữ liệu
        </Typography>

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={onSubmit}
            >
              Tạo bản sao lưu
          </Button>

          </div>
        </Card>
      </GridItem>
    </GridContainer>
  );
}

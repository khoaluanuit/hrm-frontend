import ListAccount from "components/Account/ListAccount";
import CreateAccount from "components/Account/CreateAccount";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import React from "react";


export default function AccountPage() {

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={8}>
        <ListAccount />
      </GridItem>
      <GridItem xs={12} sm={12} md={4}>
        <CreateAccount />
      </GridItem>
    </GridContainer>
      
  );
}

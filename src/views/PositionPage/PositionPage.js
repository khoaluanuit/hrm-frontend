import ListPosition from "components/Position/ListPosition";
import CreatePosition from "components/Position/CreatePosition";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import React from "react";


export default function PositionPage() {

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={8}>
        <ListPosition />
      </GridItem>
      <GridItem xs={12} sm={12} md={4}>
        <CreatePosition />
      </GridItem>
    </GridContainer>
      
  );
}

import CreateEmploye from "components/Employe/CreateEmploye";
import ListEmploye from "components/Employe/ListEmploye";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import React from "react";
export default function Icons() {
  //const dispatch = useDispatch();

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <ListEmploye />
        <CreateEmploye />
      </GridItem>
    </GridContainer>
  );
}

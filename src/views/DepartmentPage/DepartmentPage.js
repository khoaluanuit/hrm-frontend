import ListDepart from "components/Department/ListDepart";
import AddDepart from "components/Department/AddDepart";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import React from "react";


export default function DepartmentPage() {

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={8}>
        <ListDepart />
      </GridItem>
      <GridItem xs={12} sm={12} md={4}>
        <AddDepart />
      </GridItem>
    </GridContainer>
      
  );
}

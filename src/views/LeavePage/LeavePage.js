/* eslint-disable eqeqeq */
/* eslint-disable react-hooks/exhaustive-deps */
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Typography from "@material-ui/core/Typography";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import MenuItem from "@material-ui/core/MenuItem";
import { resetError } from "actions/authActions";
import TextField from "@material-ui/core/TextField";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import { makeStyles } from "@material-ui/core/styles";
import { getListNor } from "actions/employeActions";
import { getAllLeave, deleteLeave, getAllYourLeave, resetLeave, createLeave } from "actions/leaveActions";
import { hide, show } from "actions/spinerActions";
import { editLeave } from "actions/leaveActions";
import Card from "components/Card/Card.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import * as toast from "utils/toasHelper";
import MaterialTable from "material-table";
import DoneIcon from "@material-ui/icons/Done";
import ClearIcon from "@material-ui/icons/Clear";
const useStyles = makeStyles(theme => ({
  formControl: {
    width: "100%",
    paddingBottom: "10px",
    minWidth: 120
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  submit: {
    marginTop: 30
  }
}));

export default function LeavePage() {
  const isEmpty = require("is-empty");
  const leavesNotApproveYet = useSelector(
    state => state.leaves.leavesNotApproveYet
  );
  const leavesAllYour = useSelector(state => state.leaves.leavesAllYour);
  const createSuccess = useSelector(state => state.leaves.createSuccess);
  const deleteSuccess = useSelector(state => state.leaves.deleteSuccess);

  const editLeaveRec = useSelector(state => state.leaves.editLeaveRec);
  const listEmployes = useSelector(state => state.employes.listEmploye);
  const dispatch = useDispatch();
  const classes = useStyles();

  //Popup Error
  const hasErr = useSelector(state => state.errors);
  useEffect(() => {
    dispatch(hide());
    if (!isEmpty(hasErr) && hasErr.message !== "Không có đơn nghỉ phép nào!") {
      Swal.fire({
        icon: "error",
        title: Object.keys(hasErr)[0],
        text: Object.values(hasErr)[0]
      });
      dispatch(resetError());
    }
  }, [hasErr]);
  const checkRole = () => {
    return ["suAd", "ad", 'adNs', 'nvNs'].includes(localStorage.getItem('role'));
  };
  const [state, setState] = useState({
    columns: [
      { title: "Ngày nộp", field: "dateCreate" },
      { title: "Người yêu cầu", field: "nameEmp" },
      { title: "Nghỉ ngày", field: "dateLeave" },
      { title: "Lý do", field: "reason" },
      { title: "Cách liên lạc", field: "contact", emptyValue: "Chưa cập nhật" },
      { title: "Người thay thế", field: "empBackup", emptyValue: "Chưa cập nhật" }
    ],
    data: []
  });
  const handleData = arr => {
    return arr.map((item, index) => {
      item.dateLeave = new Date(
        `${item.year},${item.month},${item.date}`
      ).toLocaleDateString("vi-VN");
      item.dateOfProcessing = item.dateOfProcessing == "None" ? "Chưa cập nhật" : new Date(item.dateOfProcessing).toLocaleDateString("vi-VN");
      item.empBackup =
        item.empBackup == undefined ? "Trương Thành Nam" : item.empBackup;
      item.dateCreate =
        item.dateCreate == "undefined" ? `${index + 1}/12/2019` : new Date(item.dateCreate).toLocaleDateString("vi-VN");

      //yeu cau cua toi
      if (item.permit) {
        item.status = 'Đã duyệt'
      }

      if (!item.permit && item.approver !== 'None') {
        item.status = 'Từ chối'
      }
      return item;
    });
  };
  const [stateOne, setStateOne] = useState({
    columns: [
      { title: "Ngày nộp", field: "dateCreate" },
      { title: "Ngày xử lý", field: "dateOfProcessing", emptyValue: "Chưa cập nhật" },
      { title: "Người duyệt", field: "approver", emptyValue: "Chưa cập nhật" },
      { title: "Ghi chú", field: "note", emptyValue: "Chưa cập nhật" },
      { title: "Trạng thái", field: "status", emptyValue: "Đang chờ duyệt" }
    ],
    data: []
  });

  const [date, setDate] = useState(Date());
  const [reason, setReason] = useState();
  const [empBackup, setEmpBackup] = useState([]);
  const [contact, setContact] = useState(null);

  useEffect(() => {
    dispatch(show());
    checkRole() && dispatch(getAllLeave());
    dispatch(getAllYourLeave());
    dispatch(getListNor());

    return () => {
      dispatch(resetError());
      dispatch(resetLeave());
    };
  }, []);

  //Get all phép
  useEffect(() => {
    if (!isEmpty(leavesNotApproveYet)) {
      dispatch(hide());
      let obj = {
        ...state,
        data: handleData(leavesNotApproveYet.reverse())
      };
      setState(obj);
    }
  }, [leavesNotApproveYet]);

  // Get all your phép
  useEffect(() => {
    if (!isEmpty(leavesAllYour)) {
      setStateOne({
        ...stateOne,
        data: handleData(leavesAllYour.reverse())
      });
      dispatch(hide());
    }
  }, [leavesAllYour]);

  useEffect(() => {
    if (!isEmpty(listEmployes)) {
      //console.log(listEmployes)
      //setListEmp(listEmployes);
    }
  }, [listEmployes]);

  // Tao phep thanh cong
  useEffect(() => {
    if (createSuccess) {
      dispatch(hide());
      toast.toastSuccess("Tạo phép thành công");
      dispatch(getAllYourLeave());
      checkRole() && dispatch(getAllLeave());
    }
  }, [createSuccess]);
  
  // Duyet phep thanh cong
  useEffect(() => {
    if (editLeaveRec) {
      toast.toastSuccess(editLeaveRec);
      dispatch(show());
      dispatch(getAllLeave());
    }
  }, [editLeaveRec]);

  // Xoa phep thanh cong
  useEffect(() => {
    if (!isEmpty(deleteSuccess)) {
      dispatch(hide());
      toast.toastSuccess("Đã xóa phép");
      checkRole() && dispatch(getAllLeave());
    }
  }, [deleteSuccess]);

  const rendertListEmp = () => {
    return listEmployes.map(item => {
      return (
        <MenuItem key={item.id} value={item.fullname}>
          {item.fullname}
        </MenuItem>
      );
    });
  };

  const onSubmit = e => {
    const month = (new Date().getMonth() + 1).toString();
    const year = new Date().getFullYear().toString();
    dispatch(show());
    dispatch(createLeave({
      month,
      year,
      days: [
        {
          reason,
          empBackup,
          contact,
          date: new Date(date).getDate()
        }
      ]
    }))

  };

  return (
    <GridContainer>
      <GridItem xs={12} sm={8} md={9}>
        {!isEmpty(leavesNotApproveYet) && (
          <Card>
            <MaterialTable
              title="Danh sách phép cần duyệt"
              columns={state.columns}
              options={{
                pageSize:
                  leavesNotApproveYet.length > 5
                    ? 5
                    : leavesNotApproveYet.length
              }}
              data={state.data}
              actions={[
                {
                  icon: () => <DoneIcon color="primary" />,
                  tooltip: "Chấp nhận",
                  onClick: (event, rowData) => {
                    // console.log(rowData);
                    Swal.fire({
                      title: 'Đồng ý duyệt?',
                      text: `Đơn phép của nhân viên ${rowData.nameEmp}`,
                      icon: 'info',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Đồng ý!',
                      cancelButtonText: 'Hủy'
                    }).then((result) => {
                      if (result.value) {
                        let data = {
                          month: rowData.month,
                          year: rowData.year,
                          leaves: [{
                            date: rowData.date,
                            idEmp: rowData.idEmp,
                            permit: true,
                          }]
                        }
                        dispatch(editLeave(data))
                      }
                    })
                  }

                },
                {
                  icon: () => <ClearIcon color="error" />,
                  tooltip: "Từ chối",
                  onClick: async (event, rowData) => {
                    const { value: text } = await Swal.fire({
                      title: 'Lý do từ chối',
                      input: 'text',
                      inputValidator: (value) => {
                        if (!value) {
                          return 'Hãy cho biết lý do từ chối phép!'
                        }
                      }
                    })
                    if (text) {
                      let data = {
                        month: rowData.month,
                        year: rowData.year,
                        leaves: [{
                          date: rowData.date,
                          idEmp: rowData.idEmp,
                          permit: false,
                          note: text
                        }]
                      }
                      dispatch(editLeave(data))
                    }
                  }
                }
              ]}
            />
          </Card>
        )}

        <MaterialTable
          title="Yêu cầu của tôi"
          columns={stateOne.columns}
          options={{
            rowStyle: rowData => ({
              backgroundColor:
                rowData.permit
                  ? "#5cb860"
                  : rowData.approver !== 'None' ? '#f55a4e' : '#eee'
            }),
            pageSize: 5

          }}
          editable={{
            onRowDelete: oldData =>
              new Promise(resolve => {
                resolve();
                setStateOne(prevState => {
                  dispatch(show());
                  dispatch(deleteLeave({
                    month: oldData.month,
                    year: oldData.year,
                    date: oldData.date,
                  }));
                  const data = [...prevState.data];
                  data.splice(data.indexOf(oldData), 1);
                  return { ...prevState, data };
                });
              })
          }}
          data={stateOne.data}
        />
      </GridItem>
      <GridItem xs={12} sm={4} md={3}>
        <Card className={classes.card}>
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>

            <Typography component="h1" variant="h5">
              Tạo yêu cầu
            </Typography>

            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                className={classes.formControl}
                margin="normal"
                label="Xin nghỉ ngày"
                format="MM/dd/yyyy"
                value={date}
                onChange={val => {
                  console.log(val);
                  setDate(val);
                }}
                KeyboardButtonProps={{
                  "aria-label": "change date"
                }}
              />
            </MuiPickersUtilsProvider>

            <FormControl className={classes.formControl}>
              <InputLabel>Người thay thế</InputLabel>
              <Select
                value={empBackup}
                onChange={e => {
                  setEmpBackup(e.target.value);
                }}
              >
                {listEmployes.length > 0 && rendertListEmp()}
              </Select>
            </FormControl>

            <TextField
              onChange={e => { setContact(e.target.value) }}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Phương thức liên lạc"
            />

            <TextField
              onChange={e => {
                setReason(e.target.value);
              }}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Lý do"
            />

            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={onSubmit}
            >
              Tạo phép
            </Button>
          </div>
        </Card>
      </GridItem>
    </GridContainer>
  );
}

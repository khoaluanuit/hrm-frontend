/* eslint-disable react-hooks/exhaustive-deps */
import ClearIcon from "@material-ui/icons/Clear";
import DoneIcon from "@material-ui/icons/Done";
import { resetError } from "actions/authActions";
import { getListNor } from "actions/employeActions";
import { getAllLeave, resetLeave } from "actions/leaveActions";
import { hide, show } from "actions/spinerActions";
import MaterialTable from "material-table";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import * as toast from "utils/toasHelper";

export default function ListLeaveRequest() {
  const isEmpty = require("is-empty");
  const leavesNotApproveYet = useSelector(
    state => state.leaves.leavesNotApproveYet
  );

  const deleteSuccess = useSelector(state => state.leaves.deleteSuccess);

  const dispatch = useDispatch();
  //Popup Error
  const hasErr = useSelector(state => state.errors);
  useEffect(() => {
    dispatch(hide());
    if (!isEmpty(hasErr) && hasErr.message !== "Không có đơn nghỉ phép nào!") {
      Swal.fire({
        icon: "error",
        title: Object.keys(hasErr)[0],
        text: Object.values(hasErr)[0]
      });
      dispatch(resetError());
    }
  }, [hasErr]);
  const checkRole = () => {
    return ["suAd", "ad", 'adNs', 'nvNs'].includes(localStorage.getItem('role'));
  };
  const [state, setState] = useState({
    columns: [
      { title: "Ngày nộp", field: "dateCreate" },
      { title: "Người yêu cầu", field: "nameEmp" },
      { title: "Nghỉ ngày", field: "dateLeave" },
      { title: "Lý do", field: "reason" },
      { title: "Cách liên lạc", field: "contact", emptyValue: "Chưa cập nhật" },
      { title: "Người thay thế", field: "empBackup", emptyValue: "Chưa cập nhật" }
    ],
    data: []
  });
  const handleData = arr => {
    return arr.map((item, index) => {
      item.dateLeave = new Date(
        `${item.year},${item.month},${item.date}`
      ).toLocaleDateString("vi-VN");
      item.dateOfProcessing = item.dateOfProcessing == "None" ? "Chưa cập nhật" : new Date(item.dateOfProcessing).toLocaleDateString("vi-VN");
      item.empBackup =
        item.empBackup == undefined ? "Trương Thành Nam" : item.empBackup;
      item.dateCreate =
        item.dateCreate == "undefined" ? `${index + 1}/12/2019` : new Date(item.dateCreate).toLocaleDateString("vi-VN");

      //yeu cau cua toi
      if (item.permit) {
        item.status = 'Đã duyệt'
      }

      if (!item.permit && item.approver !== 'None') {
        item.status = 'Từ chối'
      }
      return item;
    });
  };
  const [stateOne, setStateOne] = useState({
    columns: [
      { title: "Ngày nộp", field: "dateCreate" },
      { title: "Ngày xử lý", field: "dateOfProcessing", emptyValue: "Chưa cập nhật" },
      { title: "Người duyệt", field: "approver", emptyValue: "Chưa cập nhật" },
      { title: "Ghi chú", field: "note", emptyValue: "Chưa cập nhật" },
      { title: "Trạng thái", field: "status", emptyValue: "Đang chờ duyệt" }
    ],
    data: []
  });

  useEffect(() => {
    dispatch(show());
    checkRole() && dispatch(getAllLeave());
    dispatch(getListNor());

    return () => {
      dispatch(resetError());
      dispatch(resetLeave());
    };
  }, []);

  //Get all phép
  useEffect(() => {
    if (!isEmpty(leavesNotApproveYet)) {
      setState({
        ...state,
        data: handleData(leavesNotApproveYet.reverse())
      });
    }
  }, [leavesNotApproveYet]);

  // Xoa phep thanh cong
  useEffect(() => {
    if (!isEmpty(deleteSuccess)) {
      dispatch(hide());
      toast.toastSuccess("Đã xóa phép");
      checkRole() && dispatch(getAllLeave());
    }
  }, [deleteSuccess]);

  return (
    <MaterialTable
      title="Danh sách phép cần duyệt"
      columns={state.columns}
      options={{
        pageSize:
          leavesNotApproveYet.length > 5
            ? 5
            : leavesNotApproveYet.length
      }}
      data={state.data}
      actions={[
        {
          icon: () => <DoneIcon color="primary" />,
          tooltip: "Chấp nhận",
          onClick: (event, rowData) => {
            console.log(rowData);
            Swal.fire({
              title: 'Đồng ý duyệt?',
              text: `Đơn phép của nhân viên ${rowData.nameEmp}`,
              icon: 'info',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Đồng ý!',
              cancelButtonText: 'Hủy'
            }).then((result) => {

              if (result.value) {
                Swal.fire({
                  icon: "success",
                  title: 'Đã duyệt phép'
                });
              }
            })
          }

        },
        {
          icon: () => <ClearIcon color="error" />,
          tooltip: "Từ chối",
          onClick: async (event, rowData) => {
            const { value: text } = await Swal.fire({
              title: 'Nhập lý do từ chối',
              input: 'text',
            })
            if (text) {
              Swal.fire({
                icon: "success",
                title: 'Đã từ chối phép',
                text: `Lý do: ${text}`
              });
            }
          }
        }
      ]}
    />
  );
}

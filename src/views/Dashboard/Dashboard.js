/* eslint-disable react-hooks/exhaustive-deps */
import Icon from "@material-ui/core/Icon";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Accessibility from "@material-ui/icons/Accessibility";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import Warning from "@material-ui/icons/Warning";
import { getListUnset } from "actions/accountActions";
import { getListContracts } from "actions/contractActions";
import { getListDepart } from "actions/departmentActions";
// import {

//   getList
// } from "actions/employeActions";
import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import Card from "components/Card/Card.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import ListEmploye from "components/Employe/ListEmploye";
import GridContainer from "components/Grid/GridContainer.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import Danger from "components/Typography/Danger.js";
import React, { useEffect, useState } from "react";
import { Doughnut } from 'react-chartjs-2';
import { useDispatch, useSelector } from "react-redux";
const useStyles = makeStyles(styles);

export default function Dashboard() {
  const classes = useStyles();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getListUnset());
    dispatch(getListDepart());
    dispatch(getListContracts());

    // dispatch(getList());
  }, [])
  const listCons = useSelector(state => state.cons.listCons);
  const listDeparts = useSelector((state) => state.departs.list);
  const listEmployes = useSelector(state => state.employes.listEmploye);
  const listAccUnAssign = useSelector(state => state.accounts.listAccUnset);
  const [gender, setGender] = useState({
    male: 0,
    female: 0,
    other: 0
  })
  const [religion, setReligion] = useState({
    phat: 0,
    thienchua: 0,
    caodai: 0,
    khong: 0
  })
  const [age, setAge] = useState({
    tre: 0,
    trungnien: 0,
    gia: 0
  })
  useEffect(() => {

    if (listEmployes[0] && listEmployes[0].hasOwnProperty('id')) {
      let gender = {
        male: 0,
        female: 0,
        other: 0
      }
      let regilion = {
        phat: 0,
        thienchua: 0,
        caodai: 0,
        khong: 0
      }
      let age = {
        tre: 0,
        trungnien: 0,
        gia: 0
      }
      const nowYear = new Date().getFullYear();
      listEmployes.forEach((item, index) => {
        switch (item.gender) {
          case "0":
            gender.male++;
            break;
          case "1":
            gender.female++;
            break;
          case "2":
            gender.other++;
            break;
          default:
            break;
        }
        
        switch (item.religion) {
          case "Phật Giáo":
            regilion.phat++;
            break;
          case "Cao Đài":
            regilion.caodai++;
            break;
          case "Thiên Chúa":
            regilion.thienchua++;
            break;

          default:
            regilion.khong++;
            break;
        }
        let itemYear = new Date(item.birthdate).getFullYear();
        if (nowYear - itemYear < 25){
          age.tre++
        }
        if (nowYear - itemYear >= 25 &&  nowYear - itemYear <= 35 ){
          age.trungnien++
        }
        if (nowYear - itemYear > 35){
          age.gia++
        }
      })
      // console.log(age);
      // console.log(gender);
      // console.log(regilion);
      setAge(age)
      setGender(gender);
      setReligion(regilion);
    }

  }, [listEmployes])
  // 0 Nam 1 Nữ 3 Khác
  const dataOne = {
    labels: [
      'Nam',
      'Nữ',
      'Khác'
    ],
    datasets: [{
      data: [gender.male, gender.female, gender.other],
      backgroundColor: [
        '#36A2EB',
        '#FF6384',
        '#FFCE56'
      ],
      hoverBackgroundColor: [
        '#36A2EB',
        '#FF6384',
        '#FFCE56'
      ]
    }]
  }
  const dataTwo = {
    labels: [
      'Phật',
      'Đạo',
      'Chúa',
      'Không'
    ],
    datasets: [{
      data: [religion.phat, religion.caodai, religion.thienchua, religion.khong],
      backgroundColor: [
        '#36A2EB',
        '#FF6384',
        '#FFCE56',
        '#666'
      ],
      hoverBackgroundColor: [
        '#36A2EB',
        '#FF6384',
        '#FFCE56',
        '#666'
      ]
    }]
  }
   const dataThree = {
    labels: [
      '18 - 25',
      '25 - 35',
      '35 - 55',
    ],
    datasets: [{
      data: [age.tre, age.trungnien, age.gia],
      backgroundColor: [
        '#36A2EB',
        '#FFCE56',
        '#666'
      ],
      hoverBackgroundColor: [
        '#36A2EB',
        '#FFCE56',
        '#666'
      ]
    }]
  }
  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="warning" stats icon>
              <CardIcon color="warning">
                <Icon>event_note</Icon>
              </CardIcon>
              <p className={classes.cardCategory}>Số lượng hợp đồng</p>
              <h3 className={classes.cardTitle}>
                {listCons.length}
              </h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <Danger>
                  <Warning />
                </Danger>
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="success" stats icon>
              <CardIcon color="success">
                <Icon>meeting_room</Icon>
              </CardIcon>
              <p className={classes.cardCategory}>Phòng ban</p>
              <h3 className={classes.cardTitle}>{listDeparts.length}</h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <DateRange />
                Cập nhật 24 giờ trước
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="danger" stats icon>
              <CardIcon color="danger">
                <Icon>account_circle</Icon>
              </CardIcon>
              <p className={classes.cardCategory}>Tài khoản chưa cấp</p>
              <h3 className={classes.cardTitle}>{listAccUnAssign.length}</h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <LocalOffer />
                Cập nhật 24 giờ trước
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="info" stats icon>
              <CardIcon color="info">
                <Accessibility />
              </CardIcon>
              <p className={classes.cardCategory}>Nhân viên</p>
              <h3 className={classes.cardTitle}>{listEmployes.length} người</h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <Update />
                Cập nhật theo tuần
              </div>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>

      <GridContainer>
        <GridItem xs={12} sm={12} md={4}>
          <Card>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Thống kê giới tính</h4>
            </CardHeader>
            <Doughnut data={dataOne} />
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={4}>
          <Card>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Thống kê độ tuổi</h4>
            </CardHeader>
            <Doughnut data={dataThree} />
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={4}>
          <Card>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Thống kê tôn giáo</h4>
            </CardHeader>
            <Doughnut data={dataTwo} />
          </Card>
        </GridItem>
      </GridContainer>

      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Cập nhật nhân viên</h4>
              <p className={classes.cardCategoryWhite}>
                Nhân viên mới vào trong tháng
              </p>
            </CardHeader>
            <ListEmploye max="8" />
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}

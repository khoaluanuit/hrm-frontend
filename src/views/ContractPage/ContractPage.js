import ListContract from "components/Contract/ListContract";
import CreateContract from "components/Contract/CreateContract";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import React from "react";


export default function ContractPage() {

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={9}>
        <ListContract />
      </GridItem>
      <GridItem xs={12} sm={12} md={3}>
        <CreateContract />
      </GridItem>
    </GridContainer>
      
  );
}

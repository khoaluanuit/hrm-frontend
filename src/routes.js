import BackupIcon from '@material-ui/icons/Backup';
import BubbleChart from "@material-ui/icons/BubbleChart";
import Dashboard from "@material-ui/icons/Dashboard";
import EventNote from "@material-ui/icons/EventNote";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import Person from "@material-ui/icons/Person";
import SupervisorAccountIcon from "@material-ui/icons/SupervisorAccount";
import AccountPage from "views/AccountPage/AccountPage.js";
import BackupPage from "views/BackupPage/BackupPage";
import BenefitPage from "views/BenefitPage/BenefitPage.js";
import ContractPage from "views/ContractPage/ContractPage.js";
import DashboardPage from "views/Dashboard/Dashboard.js";
import DepartmentPage from "views/DepartmentPage/DepartmentPage.js";
import EmployesPage from "views/EmployesPage/EmployesPage.js";
// import NotificationsPage from "views/Notifications/Notifications.js";
import LeavePage from "views/LeavePage/LeavePage.js";
import PositionPage from "views/PositionPage/PositionPage.js";
import UserProfile from "views/UserProfile/UserProfile.js";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Tổng quan",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin",
    role: ['suAd','ad','adNs','adHt','adKt'],
    func: ['tbtk']
  },
  
  {
    path: "/user",
    name: "Thông tin cá nhân",
    icon: Person,
    component: UserProfile,
    layout: "/admin",
    role: ['suAd','ad','nvHt','adHt','nvNs','adNs','adKt','nvKt','nv'],
    func: []
  },
  {
    path: "/employes",
    name: "Quản lý nhân viên",
    icon: BubbleChart,
    component: EmployesPage,
    layout: "/admin",
    role:['suAd','ad','nvNs','adNs'],
    func: ['qlns','qlns-1']
  },
  {
    path: "/accounts",
    name: "Quản lý tài khoản",
    icon: SupervisorAccountIcon,
    component: AccountPage,
    layout: "/admin",
    role: ['suAd','ad','nvHt','adHt'],
    func: ['qlht','qlht-1']
  },
  {
    path: "/contracts",
    name: "Quản lý hợp đồng",
    icon: EventNote,
    component: ContractPage,
    layout: "/admin",
    role: ['suAd','ad','nvNs','adNs'],
    func: ['qlns','qlns-1']
  },
  {
    path: "/positions",
    name: "Quản lý vị trí",
    icon: BubbleChart,
    component: PositionPage,
    layout: "/admin",
    role: ['suAd','ad'],
    func: ['qlns','qlns-1']
  },
  {
    path: "/departments",
    name: "Quản lý phòng ban",
    icon: LibraryBooks,
    component: DepartmentPage,
    layout: "/admin",
    role: ['suAd','ad'],
    func: ['qlns','qlns-1']
  },
  // {
  //   path: "/table",
  //   name: "Lương thưởng",
  //   icon: "content_paste",
  //   component: TableList,
  //   layout: "/admin",
  //   role: ['suAd','ad','nvHt','adHt','nvNs','adNs','adKt','nvKt','nv'],
  //   func: ['qlkt','qlkt-1']
  // },

  {
    path: "/benefit",
    name: "Chấm công",
    icon: LocationOn,
    component: BenefitPage,
    layout: "/admin",
    role: ['suAd','ad','nvHt','adHt','nvNs','adNs','adKt','nvKt','nv'],
    func: []
  },
  {
    path: "/leave",
    name: "Nghỉ phép",
    icon: Notifications,
    component: LeavePage,
    layout: "/admin",
    role: [],
    func: []
  },{
    path: "/backup",
    name: "Sao lưu phục hồi",
    icon: BackupIcon,
    component: BackupPage,
    layout: "/admin",
    role: ['suAd','ad','adHt','nvHt'],
    func: ['qlht']
  }
];

export default dashboardRoutes;

import React, { Component } from "react";
class Footer extends Component {
    render() {
        return (
            <footer className="page-footer font-small">
                <div className="center">
                    <hr className="linefooter" />
                    <span>Design by John Trinh</span>
                </div>
            </footer>
        );
    }
}
export default Footer;
/* eslint-disable react-hooks/exhaustive-deps */
import AppBar from "@material-ui/core/AppBar";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import Slide from "@material-ui/core/Slide";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import CloseIcon from "@material-ui/icons/Close";
import { addSession, verifyUserAct, resetPassUserAct, resetDelAcc } from "actions/accountActions";
import { logoutUser, resetError } from "actions/authActions";
import { resetDialog } from "actions/employeActions";
import Card from "components/Card/Card.js";
import firebase from 'components/firebase';
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import * as toast from "utils/toasHelper";
import { show, hide } from "actions/spinerActions";
import ContactMailIcon from '@material-ui/icons/ContactMail';


const useStyles = makeStyles(theme => ({
  avatarSty: {
    width: 60,
    height: 60,
    margin: "auto",
    marginBottom: 30,
    backgroundColor: theme.palette.secondary.main
  },
  appBar: {
    position: "relative"
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  input: {
    textAlign: 'center',
    // width: 277,
    fontSize: 20,
    fontWeight: 500
  },
  cardPass: {
    display: "flex",
    alignItems: "center",
    boxShadow: 'unset'
  },
  submit: {
    marginTop: 20
  }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
function getSteps() {
  return ['Xác thực email để gửi yêu cầu', 'Kiểm tra email nhận mã reset', 'Kiểm tra email nhận mật khẩu mới'];
}
export default function DialogReset({ ...props }) {
  const classes = useStyles();
  const dispatch = useDispatch()
  const [activeStep, setActiveStep] = useState(0);
  const steps = getSteps();

  const verifyUser = useSelector(state => state.accounts.verifyUser);
  useEffect(() => {
    if (!isEmpty(verifyUser)) {
      dispatch(hide());
      toast.toastSuccess(verifyUser);
      setActiveStep(prevActiveStep => prevActiveStep + 1);
    }
  }, [verifyUser]);

  const resetPassUser = useSelector(state => state.accounts.resetPassUser);
  useEffect(() => {
    if (!isEmpty(resetPassUser)) {
      dispatch(hide());
      setActiveStep(prevActiveStep => prevActiveStep + 1);
      setTimeout(() => {
        Swal.fire({
          icon: "success",
          title: "Mật khẩu đã được thay đổi"
        }).then(() => {
          handleClose();
        });
      }, 500);
    }
  }, [resetPassUser]);

  const handleNext = () => {
    if (activeStep === 0) {
      // dispatch(show());
      dispatch(verifyUserAct({ email }));
    }

    if (activeStep === 1) {
      // dispatch(show());
      dispatch(resetPassUserAct({ email,otp }));
    }
    // setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const isEmpty = require("is-empty");
  const [open, setOpen] = React.useState(false);
  const [isClicked, setIsClicked] = useState("");
  const [title, setTitle] = React.useState(null);
  const dialogFull = useSelector(state => state.employes.dialogFullMail);

  //Password
  const [otp, setOTP] = React.useState("");
  const [email, setEmail] = React.useState("");

  useEffect(() => {
    if (!isEmpty(dialogFull)) {
      setOpen(true);
      setTitle(dialogFull.title);
      dispatch(resetDialog());
    }
  }, [dialogFull]);

  useEffect(() => {
    if (open) {
      firebase.renderCaptchaMail(() => { setIsClicked(true) });
    }

  }, [open])

  const handleClose = () => {

    (isClicked || email.length > 0 ) &&
      Swal.fire({
        title: 'Rời khỏi trang ?',
        text: `Các thay đổi trên trang sẽ không được lưu lại.`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Đồng ý!',
        cancelButtonText: 'Hủy'
      }).then((result) => {
        result.value && setOpen(false)
        dispatch(hide())
      })

  };

  const hasErr = useSelector(state => state.errors);
  //Popup Error

  useEffect(() => {
    if (Object.keys(hasErr).length !== 0 && hasErr.constructor === Object) {
      Swal.fire({
        icon: "error",
        title: Object.keys(hasErr)[0],
        text: Object.values(hasErr)[0]
      });
      dispatch(resetError());
    }
  }, [hasErr]);


  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6">{title}</Typography>
          </Toolbar>
        </AppBar>

        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map(label => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>

        <div>
          <Card className={classes.cardPass}>
            <div className={classes.paper}>
              <Avatar className={classes.avatarSty}>
                <ContactMailIcon style={{ height: "1.5em", width: "1.5em" }} />
              </Avatar>

              {
                activeStep == 0 && <div>
                  <TextField
                    onChange={e => {
                      setEmail(e.target.value);
                    }}

                    inputProps={{
                      className: classes.input
                    }}
                    value={email}
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    label="Email"
                  />
                  <div id="recaptcha-reset-mail"></div>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    disabled={email.length < 7 || !isClicked}
                    onClick={handleNext}
                  >
                    Xác nhận
                            </Button>
                </div>

              }
              {
                activeStep == 1 && <div>
                  <div>Nhập mã OTP chứa trong Email để xác thực</div>
                  <TextField
                    onChange={e => {
                      setOTP(e.target.value);
                    }}

                    inputProps={{
                      className: classes.input
                    }}
                    value={otp}
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    label="Mã xác thực"
                  />
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    disabled={otp.length < 6}
                    onClick={handleNext}
                  >
                    Xác nhận
                            </Button>
                </div>
              }


            </div>
          </Card>

        </div>
      </Dialog>
    </div>
  );
}

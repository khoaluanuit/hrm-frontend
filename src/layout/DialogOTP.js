/* eslint-disable react-hooks/exhaustive-deps */
import AppBar from "@material-ui/core/AppBar";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import Slide from "@material-ui/core/Slide";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import ScreenLockPortraitIcon from '@material-ui/icons/ScreenLockPortrait';
import { addSession } from "actions/accountActions";
import { logoutUser, resetError } from "actions/authActions";
import { resetDialog } from "actions/employeActions";
import Card from "components/Card/Card.js";
import firebase from 'components/firebase';
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import * as toast from "utils/toasHelper";

const useStyles = makeStyles(theme => ({
  avatarSty: {
    width: 60,
    height: 60,
    margin: "auto",
    marginBottom: 30,
    backgroundColor: theme.palette.secondary.main
  },
  appBar: {
    position: "relative"
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  input: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 500
  },
  cardPass: {
    display: "flex",
    alignItems: "center",
    boxShadow: 'unset'
  }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function DialogOTP({ ...props }) {
  const classes = useStyles();
  const dispatch = useDispatch()

  const isEmpty = require("is-empty");
  const [open, setOpen] = React.useState(false);
  const [title, setTitle] = React.useState(null);
  const [data, setData] = React.useState({});
  const dialogFull = useSelector(state => state.employes.dialogFull);

  //Password
  const [otp, setOTP] = React.useState("");

  useEffect(() => {
    if (!isEmpty(dialogFull)) {
      setOpen(true);
      setTitle(dialogFull.title);
      setData(dialogFull.data);
      !isEmpty(dialogFull.data) && setData(dialogFull.data);
      dispatch(resetDialog());
    }
  }, [dialogFull]);


  const handleClose = () => {
    Swal.fire({
      title: 'Rời khỏi trang ?',
      text: `Các thay đổi trên trang sẽ không được lưu lại.`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Đồng ý!',
      cancelButtonText: 'Hủy'
    }).then((result) => {
      result.value && setOpen(false)

    })

  };

  const hasErr = useSelector(state => state.errors);
  //Popup Error

  useEffect(() => {
    if (Object.keys(hasErr).length !== 0 && hasErr.constructor === Object) {
      Swal.fire({
        icon: "error",
        title: Object.keys(hasErr)[0],
        text: Object.values(hasErr)[0]
      });
      dispatch(resetError());
    }
  }, [hasErr]);
  const onSubmit = () => {
    firebase.confirmOTP(otp, function (res) {
      //false
      if (res.hasOwnProperty('message')) {
        setOTP("");
        dispatch(logoutUser())
        toast.toastError(` 😱Mã xác thực không đúng. Xin thử lại!`);
      } else {
        dispatch(addSession())
      }
    });
  }
  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6">{title}</Typography>
          </Toolbar>
        </AppBar>
        <div>
          <Card className={classes.cardPass}>
            <div className={classes.paper}>
              <Avatar className={classes.avatarSty}>
                <ScreenLockPortraitIcon style={{ height: "1.5em", width: "1.5em" }} />
              </Avatar>
              <div style={{ color: 'gray' }}>* Mã OTP đã được gửi về điện thoại của bạn. </div>
              <div style={{ color: 'gray' }}>* Kiểm tra điện thoại có đuôi <span style={{ color: 'red' }}>{data.phone}</span> và thực hiện bước xác nhận</div>
              <TextField
                onChange={e => {
                  if (e.target.value.length < 7) {
                    setOTP(e.target.value);
                  }
                }}

                inputProps={{
                  maxLength: 6,
                  className: classes.input
                }}
                value={otp}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                label="Mã xác thực"
                type="number"
              />

              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                disabled={otp.length < 6}
                onClick={onSubmit}
              >
                Xác nhận
          </Button>
            </div>
          </Card>

        </div>
      </Dialog>
    </div>
  );
}

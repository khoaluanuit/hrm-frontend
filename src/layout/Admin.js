/* eslint-disable react-hooks/exhaustive-deps */
import { makeStyles } from "@material-ui/core/styles";
import { getListRole } from "actions/accountActions";
import logo from "assets/img/reactlogo.png";
import bgImage from "assets/img/sidebar-2.jpg";
import styles from "assets/jss/material-dashboard-react/layouts/adminStyle.js";
import Footer from "components/Footer/Footer.js";
// core components
import Navbar from "components/Navbars/Navbar.js";
import Sidebar from "components/Sidebar/Sidebar.js";
// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
import React from "react";
import { Animated } from "react-animated-css";
import { useDispatch, useSelector } from "react-redux";
import { Route, Switch } from "react-router-dom";
// import { useEffect, useState } from "react";
import routes from "routes.js";


let ps;

const switchRoutes = (
  <Switch>
    {routes.map((prop, key) => {
      if (prop.layout === "/admin") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      }
      return null;
    })}
  </Switch>
);

const useStyles = makeStyles(styles);

export default function Admin({ ...rest }) {
  // styles
  const classes = useStyles();
  const dispatch = useDispatch();

  // ref to help us initialize PerfectScrollbar on windows devices
  const mainPanel = React.createRef();
  // states and functions
  const [image] = React.useState(bgImage);
  const [color] = React.useState("blue");

  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const getRoute = () => {
    return true;
    //return window.location.pathname !== "/admin/maps";
  };
  const resizeFunction = () => {
    if (window.innerWidth >= 960) {
      setMobileOpen(false);
    }
  };


  React.useEffect(() => {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(mainPanel.current, {
        suppressScrollX: true,
        suppressScrollY: false
      });
      document.body.style.overflow = "hidden";
    }
    window.addEventListener("resize", resizeFunction,{passive: true});
    // Specify how to clean up after this effect:
    return function cleanup() {
      if (navigator.platform.indexOf("Win") > -1) {
        ps.destroy();
      }
      window.removeEventListener("resize", resizeFunction,{passive: true});
    };
  }, [mainPanel]);
  const listFunc = useSelector(state => state.accounts.listRoleFuc);
  React.useEffect(() => {
    dispatch(getListRole());
  }, []);

  const [listRole, setListRole] = React.useState(['qlmd']);
  React.useEffect(() => {
    setListRole(listFunc);    
  }, [listFunc]);

  return (
    <div className={classes.wrapper}>
      <Animated
          animationIn="fadeInLeft"
          animationOut="fadeOutLeft"
          animationInDuration={500}
          animationOutDuration={1000}
          style={{height: '100%', width: ' 260px', display: 'inline-block'}}
        >
          <Sidebar
            routes={routes.filter(item => {
              if (item.func.length === 0){
                return true;
              }
              let isCheck = false;
              listRole.forEach(item1 => {
                if (!isCheck) {
                  isCheck = item.func.includes(item1.idFunc);
                }
              });
              return isCheck;
            })}
            logoText={"HRM-2019"}
            logo={logo}
            image={image}
            handleDrawerToggle={handleDrawerToggle}
            open={mobileOpen}
            color={color}
            {...rest}
          />
        </Animated>

      <div className={classes.mainPanel} ref={mainPanel}>
        <Navbar
          routes={routes}
          handleDrawerToggle={handleDrawerToggle}
          {...rest}
        />

        {getRoute() ? (
          <Animated
          animationIn="fadeIn"
          animationInDelay={1000}
          animationInDuration={1000}
          className={classes.content}
          style={{opacity: 0}}
        >
          <div className={classes.container}>{switchRoutes}</div>
        </Animated>

        ) : (
          <div className={classes.map}>{switchRoutes}</div>
        )}

        {getRoute() ? <Footer /> : null}
      </div>
    </div>
  );
}

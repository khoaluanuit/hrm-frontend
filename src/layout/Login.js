/* eslint-disable react-hooks/exhaustive-deps */
import Avatar from "@material-ui/core/Avatar";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { addSession } from "actions/accountActions";
import { loginUser, resetError, resetRegister, setCurrentUser } from "actions/authActions";
import { showDialog, showDialogMail } from "actions/employeActions";
import firebase from 'components/firebase';
import isEmpty from "is-empty";
import jwt_decode from "jwt-decode";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import axiosService from 'utils/axiosService';
import DialogOTP from "./DialogOTP";
import DialogReset from "./DialogReset";
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        uit.edu.vn
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh"
  },
  image: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center"
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));


export default function SignInSide({ ...rest }) {
  const classes = useStyles();
  //namtt

  const [email, setEmail] = useState("");
  const [password, setpassword] = useState("");
  const [isClicked, setIsClicked] = useState("");
  const dispatch = useDispatch();
  const islogin = useSelector((state) => state.auth.isAuthenticated);
  const hasErr = useSelector((state) => state.errors);

  //Login if have token
  islogin && rest.history.push("/admin/dashboard");

  if (Object.keys(hasErr).length !== 0 && hasErr.constructor === Object) {
    Swal.fire({
      icon: "error",
      title: Object.keys(hasErr)[0],
      text: Object.values(hasErr)[0]
    });
    dispatch(resetError());
  }

  useEffect(() => {
    firebase.renderCaptcha(() => { setIsClicked(true) });
    return () => {
      dispatch(resetRegister());
      axiosService.setToken(localStorage['x-auth-token']);
    }
  }, [])

  const onSubmit = (e) => {
    e.preventDefault();
    dispatch(loginUser({ email, password }));
  };
  const loginSuccess = useSelector((state) => state.auth.loginSuccess);

  const session = useSelector((state) => state.accounts.session);

  useEffect(() => {
    if (!isEmpty(session)) {
      const { token } = loginSuccess;
      const decoded = jwt_decode(token);
      localStorage.setItem("x-auth-token", token);
      localStorage.setItem('account', decoded.email)
      localStorage.setItem('role', decoded.role)
      dispatch(setCurrentUser(decoded));

      // Detect role redirect
      if (['suAd', 'ad', 'adNs', 'adHt', 'adKt'].includes(decoded.role)) {
        rest.history.push("/admin/dashboard")
      } else {
        rest.history.push("/admin/user")
      }
    }
  }, [session]);

  useEffect(() => {
    if (!isEmpty(loginSuccess)) {
      const { token } = loginSuccess;
      const decoded = jwt_decode(token);

      if (decoded.auth2) {
        firebase.phoneAuth(decoded);
        dispatch(showDialog({ title: "Xác thực OTP", data: decoded }))

      } else {
        // Khong can OTP
        dispatch(addSession())
      }
    }
  }, [loginSuccess]);

  const isValid = () => {
    if (email.length > 0 && password.length > 0 && isClicked) {
      return false;
    }
    return true
  }
  const onClickReset = ()=>{
    dispatch(showDialogMail({ title: "Reset mật khẩu" }))
  }
  return (

    <Grid container component="main" className={classes.root}>
      <div className={classes.searchWrapper}>
        <DialogOTP />
        <DialogReset/>
      </div>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={8} className={classes.image} />
      <Grid item xs={12} sm={8} md={4} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Đăng nhập
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Địa chỉ Email"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              onChange={(e) => {
                setpassword(e.target.value);
              }}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Mật khẩu"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <div id="recaptcha-container"></div>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={isValid()}
              onClick={(e) => {
                onSubmit(e);
              }}
            >
              Đăng nhập
            </Button>

            <Grid container>
              <Grid item xs>
              </Grid>
              <Grid item>
                <Link href="#" variant="body2" onClick={onClickReset}>
                  Quên mật khẩu?
                </Link>
              </Grid>
            </Grid>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}

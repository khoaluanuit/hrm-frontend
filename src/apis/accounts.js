import axiosService from '../utils/axiosService';
import { API_ENDPOINT } from '../constants';

const url = 'accounts';

export const logInUser = (params = {}) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/login`,params);
};

export const setSession = () => {
    return axiosService.post(`${API_ENDPOINT}/${url}/session`);
};

export const register = (params = {}) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/register`,params);
};

export const getListAccUnAssign = () => {
    return axiosService.get(`${API_ENDPOINT}/${url}/employee`);
};
export const getListAcc = () => {
    return axiosService.get(`${API_ENDPOINT}/${url}`);
};

export const delAcc = (id) => {
    return axiosService.delete(`${API_ENDPOINT}/${url}/delete/${id}`);
};

export const editAcc = (data) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/edit/${data.id}`, data);
};

export const editPass = (data) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/edit/yourinfo/password`, data);
};

export const getListRoleFunc = () => {
    return axiosService.get(`${API_ENDPOINT}/${url}/account/role`);
};
export const resetPassAd = (id) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/edit/password/${id}`);
};

export const verifyUser = (data) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/verify`,data);
};

export const resetPassUser = (data) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/resetYourPassword`,data);
};

// /accounts/edit/password/:id
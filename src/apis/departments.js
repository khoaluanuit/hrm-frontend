import axiosService from '../utils/axiosService';
import { API_ENDPOINT } from '../constants';

const url = 'departments';

export const getList = () => {
    return axiosService.get(`${API_ENDPOINT}/${url}`);
};

export const addDepart = (data) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/create`, data);
};

export const deleteDept = (id) => {
    return axiosService.delete(`${API_ENDPOINT}/${url}/delete/${id}`);
};

export const editById = (data) => {
    console.log(data); 
    return axiosService.post(`${API_ENDPOINT}/${url}/edit/${data._id}`, data);
};
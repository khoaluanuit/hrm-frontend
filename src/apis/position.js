import axiosService from "../utils/axiosService";
import { API_ENDPOINT } from "../constants";

const url = "positions";

export const getList = (params = {}) => {
  return axiosService.get(`${API_ENDPOINT}/${url}`);
};

export const delById = (params = {}) => {
  return axiosService.delete(`${API_ENDPOINT}/${url}/delete/${params}`);
};

export const addPos = (params = {}) => {
  return axiosService.post(`${API_ENDPOINT}/${url}/create`, params);
};

export const editById = (params = {}) => {
  return axiosService.post(`${API_ENDPOINT}/${url}/edit/${params.id}`, params);
};

export const getPosByDepart = (params = {}) => {
  return axiosService.post(`${API_ENDPOINT}/${url}/departments`, params);
};

export const getPosByNameDepart = (params = {}) => {
  return axiosService.post(`${API_ENDPOINT}/${url}/departs`, params);
};

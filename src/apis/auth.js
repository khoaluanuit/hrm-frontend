import axiosService from '../utils/axiosService';
import { API_ENDPOINT } from '../constants';

const url = 'accounts';

export const register = (params = {}) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/register`);
};

export const logout = (params = {}) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/logout`);
};
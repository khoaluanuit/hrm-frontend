import axiosService from '../utils/axiosService';
import { API_ENDPOINT } from '../constants';

const url = 'leaves';

export const getAll = (params = {}) => {
    return axiosService.get(`${API_ENDPOINT}/${url}`);
};

export const delLeave = (params = {}) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/delete`,params);
};

export const approveLeave = (params = {}) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/approve`,params);
};

export const getAllYour = (params = {}) => {
    return axiosService.get(`${API_ENDPOINT}/${url}/yourleaves`);
};

export const create = (params = {}) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/create`, params);
};


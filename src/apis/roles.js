import axiosService from '../utils/axiosService';
import { API_ENDPOINT } from '../constants';

const url = 'roles';

export const getList = (params = {}) => {
    return axiosService.get(`${API_ENDPOINT}/${url}`);
};

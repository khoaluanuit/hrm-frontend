import axiosService from '../utils/axiosService';
import { API_ENDPOINT } from '../constants';

const url = 'backupRestores';

export const getAllBack = (params = {}) => {
    return axiosService.get(`${API_ENDPOINT}/${url}`);
};

export const deleteBack = (params = {}) => {
    return axiosService.delete(`${API_ENDPOINT}/${url}/delete/${params}`);
};

export const createBack = (params = {}) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/backup`, params);
};

export const restoreBack = (params = {}) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/restore/${params}`);
};



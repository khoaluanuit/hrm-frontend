import axiosService from '../utils/axiosService';
import { API_ENDPOINT } from '../constants';

const url = 'employees';

export const getListEmp = () => {
    return axiosService.get(`${API_ENDPOINT}/${url}`);
};

export const getListNormalEmp = () => {
    return axiosService.get(`${API_ENDPOINT}/${url}/nomalUser`);
};

export const getYourInfor = () => {
    return axiosService.get(`${API_ENDPOINT}/${url}/employee/yourinfo`);
};

export const addEmploye = (data) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/create`, data);
};

export const editEmpl = (data) => { 
    return axiosService.post(`${API_ENDPOINT}/${url}/edit/${data.id}`,data);
};

export const editEmplNormal = (data) => { 
    return axiosService.post(`${API_ENDPOINT}/${url}/edit/employee/yourinfo`,data);
};

export const delEmpl = (id) => {
    return axiosService.delete(`${API_ENDPOINT}/${url}/delete/${id}`);
};
import axiosService from "../utils/axiosService";
import { API_ENDPOINT } from "../constants";

const url = "timekeepings";


export const getYourMonsById = () => {
    return axiosService.get(`${API_ENDPOINT}/${url}/months`);
};

export const getYourKeep = (params = {}) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/month/yourtimekeeping`,params);
};
export const getKeepById = (params = {}) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/month/employee`,params);
};

export const editById = (params = {}) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/edit/${params.id}`, params);
};

export const createById = (params = {}) => {
    return axiosService.post(`${API_ENDPOINT}/${url}/create/employee`, params);
};

export const createMonth = () => {
    return axiosService.post(`${API_ENDPOINT}/${url}/create`);
};
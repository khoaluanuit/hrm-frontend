export const GET_LIST = "GET_LIST";
export const ADD_DEPART = "ADD_DEPART";
export const DELETE_DEPART = "DELETE_DEPART";
export const RESET_DELETE = "RESET_DELETE";
export const EDIT_DEPART = "EDIT_DEPART";
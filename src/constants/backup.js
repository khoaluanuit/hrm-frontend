export const GET_LIST_BACKUP = "GET_LIST_BACKUP";
export const CREATE_BACKUP = "CREATE_BACKUP";
export const DELETE_BACKUP = "DELETE_BACKUP";
export const RESET_BACKUP = "RESET_BACKUP";
export const RESTORE_BACKUP = "RESTORE_BACKUP";

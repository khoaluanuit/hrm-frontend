// employes
export const GET_ERRORS = "GET_ERRORS";
export const CREATE_USER = "CREATE_USER";
export const RESET_DATA = "RESET_DATA";

export const GETLIST_SUCCESS = "GETLIST_SUCCESS";
export const GET_YOUR_EMPLOY = "GET_YOUR_EMPLOY";
export const GET_YOUR_ACCOUNT = "GET_YOUR_ACCOUNT";


//delete
export const DEL_EMPLOYE = "DEL_EMPLOYE";
export const RESET_DEL_EMPLOYE = "RESET_DEL_EMPLOYE";
export const EDIT_EMPLOYE = "EDIT_EMPLOYE";
export const ON_EDIT_EMPLOYE = "ON_EDIT_EMPLOYE";

// accounts
export const GETLIST_UNSET = "GETLIST_UNSET";
export const GETLIST_ACC = "GETLIST_ACC";
export const DELETE_ACC = "DELETE_ACC";
export const RESET_ACCOUNTS = "RESET_ACCOUNTS";
export const EDIT_ACCOUNT = "EDIT_ACCOUNT";
export const GET_LIST_ROLE_FUNC = "GET_LIST_ROLE_FUNC";
export const CHANGE_PASS = "CHANGE_PASS";
export const SET_SESSION = "SET_SESSION";
export const RESET_PASS_ADMIN = "RESET_PASS_ADMIN";
export const RESET_PASS_USER = "RESET_PASS_USER";
export const VERIFY_USER = "VERIFY_USER";
//dialog
export const SHOW_FULL_DIALOG = "SHOW_FULL_DIALOG";
export const SHOW_FULL_DIALOG_MAIL = "SHOW_FULL_DIALOG_MAIL";
export const RESET_DIALOG = "RESET_DIALOG";

//Loading spinner
export const DATA_LOADING = "DATA_LOADING";
export const DATA_DONE = "DATA_DONE";
//Dialog
export const SHOW_DIALOG = "SHOW_DIALOG";
export const HIDE_DIALOG = "HIDE_DIALOG";

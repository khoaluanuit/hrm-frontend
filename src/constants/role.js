export const GET_LIST_ROLE = "GET_LIST_ROLE";
export const ADD_ROLE = "ADD_ROLE";
export const DELETE_ROLE = "DELETE_ROLE";
export const RESET_ROLE = "RESET_ROLE";

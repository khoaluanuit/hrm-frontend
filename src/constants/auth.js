

export const GET_ERRORS = "GET_ERRORS";
export const SET_CURRENT_USER = "SET_CURRENT_USER";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const GET_USERS = "GET_USERS";
export const GET_USER = "GET_USER";

export const RESET_DATA = "RESET_DATA";
export const RESET_ERRORS = "RESET_ERRORS";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const RESET_REGISTER = "RESET_REGISTER";



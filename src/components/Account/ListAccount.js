/* eslint-disable react-hooks/exhaustive-deps */
import { delAccount, editList, getList, resetDelAcc, resetPassAdminAct } from "actions/accountActions";
import { getListRole } from "actions/roleActions";
import MaterialTable from "material-table";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as toast from "utils/toasHelper";
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import Swal from "sweetalert2";

export default function ListAccounts() {
  const listAccounts = useSelector(state => state.accounts.listAcc);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    columns: [
      { title: "Tên", field: "username" },
      { title: "Email", field: "email" },
      { title: "Role", field: "role" },
      { title: "Xác thực OTP", field: "auth2" },
      { title: "Trạng thái", field: "active" },
    ],
    data: []
  });

  //Get list Role
  const listRoles = useSelector(state => state.roles.listRoles);
  useEffect(() => {
    dispatch(getList());
    dispatch(getListRole());
  }, []);

  useEffect(() => {
    setState({ ...state, data: listAccounts });
  }, [listAccounts]);

  useEffect(() => {
    const lookup = {};

    // console.log(listAccounts);
    listRoles.forEach(element => {
      lookup[element.name] = element.name;
    });

    setState({
      ...state,
      columns: [
        { title: "Tên", field: "username" },
        { title: "Email", field: "email", editable: "never" },
        { title: "Role", field: "role", lookup },
        { title: "OTP", field: "auth2", lookup: { true: 'ON', false: 'OFF' } },
        { title: "Trạng thái", field: "active", lookup: { true: 'Kích hoạt', false: 'Khóa' } },
      ]
    });
  }, [listRoles]);

  const resetPassAdmin = useSelector(state => state.accounts.resetPassAdmin);
  useEffect(() => {
    if (resetPassAdmin.length !== 0) {
      toast.toastSuccess(resetPassAdmin);
    }
  }, [resetPassAdmin]);

  // Delete Accounts
  const deleteuser = useSelector(state => state.accounts.deleteuser);
  useEffect(() => {
    if (deleteuser.length !== 0) {
      toast.toastSuccess(`Xóa tài khoản thành công`);
      dispatch(resetDelAcc());
    }
  }, [deleteuser]);

  // Edit Accounts
  const editAccount = useSelector(state => state.accounts.editAccount);
  useEffect(() => {
    if (editAccount.length !== 0) {
      toast.toastSuccess(`Sửa thông tin thành công`);
      dispatch(resetDelAcc());
    }
  }, [editAccount]);

  return (
    <MaterialTable
      title="Danh sách tài khoản"
      columns={state.columns}
      options={{
        pageSize: 10,
        exportFileName: "Danh sách tài khoản",
        exportAllData: true,
        exportButton: true
      }}
      localization={{
        toolbar: {
          exportName: "Xuất file exel",
          exportTitle: "Xuất file exel"
        }
      }}
      data={state.data}
      // onRowClick={(e, data) => {
      //   console.log(data);
      // }}
      actions={[{
        icon: () => <VpnKeyIcon color="primary" />,
        tooltip: "Reset mật khẩu",
        onClick: (event, rowData) => {
          console.log(rowData); 
          Swal.fire({
            title: 'Bạn có chắc?',
            text: `Reset mật khẩu của ${rowData.username}`,
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Đồng ý!',
            cancelButtonText: 'Hủy'
          }).then((result) => {
            if (result.value) {
              dispatch(resetPassAdminAct(rowData.id))
            }
          })
        }

      }]
      }
      editable={{
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            let tempObj = { ...newData };
            listRoles.forEach(item => {
              if (tempObj.role === item.name) {
                tempObj.role = item.idRo;
              }
            });

            setTimeout(() => {
              setState(prevState => {

                if (
                  oldData.username !== tempObj.username ||
                  oldData.role !== tempObj.role
                ) {
                  console.log(newData);
                  tempObj.auth2 = newData.auth2;
                  tempObj.active = newData.active;

                  dispatch(editList(tempObj));
                }
                const data = [...prevState.data];
                const index = data.indexOf(oldData);
                data[index] = newData;
                return { ...prevState, data };
              });
              resolve();
            }, 500);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            resolve();
            setState(prevState => {
              dispatch(delAccount(oldData.id));
              const data = [...prevState.data];
              data.splice(data.indexOf(oldData), 1);
              return { ...prevState, data };
            });
          })
      }}
    />
  );
}

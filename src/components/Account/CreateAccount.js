/* eslint-disable react-hooks/exhaustive-deps */
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { logoutUser, registerUser, resetError, resetRegister } from "actions/authActions";
import { getListRole } from 'actions/roleActions';
import { hide } from 'actions/spinerActions';
import background from 'assets/img/register/background.jpg';
import Card from "components/Card/Card.js";
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import Swal from 'sweetalert2';
const useStyles = makeStyles(theme => ({
  card: {
    marginTop: '0 !important'
  },
  formControl: {
    width: "100%",
    margin: "27px 0 0 0",
    paddingBottom: "10px",
    minWidth: 120
  },
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: `url(${background})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function CreateAccount({ ...rest }) {

  const classes = useStyles();
  //namtt
  const [email, setEmail] = useState("")
  const [role, setRole] = useState("nv")
  const [password, setpassword] = useState("")
  const [username, setName] = useState("")
  const [password2, setpassword2] = useState("")
  const dispatch = useDispatch();

  const hasErr = useSelector(state => state.errors)
  const isRegisSuccess = useSelector(state => state.auth.isRegisSuccess)

  //Get list Role
  const listRoles = useSelector(state => state.roles.listRoles)
  useEffect(() => {
    dispatch(getListRole());
  }, [])

  const rendertListRoles = () => {
    return listRoles.map((item) => {
      return <MenuItem key={item._id} value={item.idRo}>{item.name}</MenuItem>
    })
  }

  //Popup Error
  if (Object.keys(hasErr).length !== 0 && hasErr.constructor === Object) {
    Swal.fire({
      icon: 'error',
      title: Object.keys(hasErr)[0],
      text: Object.values(hasErr)[0]
    })
    dispatch(resetError());
    dispatch(hide());
  }

  // Popup Success
  useEffect(() => {

    if (isRegisSuccess) {
      Swal.fire({
        title: 'Đăng ký thành công',
        text: "Chuyển sang trang đăng nhập ?",
        icon: 'success',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        cancelButtonText: 'Không',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Đăng nhập lại'
      }).then((result) => {
        if (result.value) {
          dispatch(logoutUser());
        }
        dispatch(resetRegister());
        dispatch(hide());
      })

    }
  }, [isRegisSuccess])

  const onSubmit = ((e) => {
    e.preventDefault();
    dispatch(registerUser({ username, email, password, password2, role }))
  });


  return (
    <Card className={classes.card}>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Đăng ký
      </Typography>
        <form className={classes.form} noValidate>
          <TextField
            onChange={(e) => { setName(e.target.value) }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Tên tài khoản"
            name="username"
            autoComplete="username"
            autoFocus
          />
          <TextField
            onChange={(e) => { setEmail(e.target.value) }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Địa chỉ Email"
            name="email"
            type="email"
            autoComplete="email"
            autoFocus
          />
          <TextField
            onChange={(e) => { setpassword(e.target.value) }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Mật khẩu"
            type="password"
            name="password"
            autoComplete="password"
            autoFocus
          />
          <TextField
            onChange={(e) => { setpassword2(e.target.value) }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password2"
            label="Nhập lại mật khẩu"
            type="password"
            autoComplete="password2"
          />
          <FormControl className={classes.formControl} >
            <InputLabel>Vai trò</InputLabel>
            <Select
              value={role}
              onChange={(e) => { setRole(e.target.value) }}
            >
              {listRoles.length > 0 && rendertListRoles()}
            </Select>
          </FormControl>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={(e) => { onSubmit(e) }}
          >
            Đăng ký
        </Button>

        </form>
      </div>
    </Card>

  );
}

/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable eqeqeq */
import { delEmploye, getList, getListNor, onEditEmloy, resetEmloy } from "actions/employeActions";
import { hide } from "actions/spinerActions";
import MaterialTable from "material-table";
import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as toast from "utils/toasHelper";
import Dialog from "./Dialog";
import EditEmploye from "./EditEmploye";

export default function ListEmploye({...prop}) {
  const listEmployes = useSelector(state => state.employes.listEmploye);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    columns: [
      { title: "ID", field: "idEmp" },
      { title: "Tên", field: "fullname" },
      { title: "Điện thoại", field: "phone", emptyValue: "Chưa có thông tin" },
      { title: "Email", field: "email", emptyValue: "Chưa có thông tin" },
      { title: "Hợp đồng", field: "contract" },
      {
        title: "Phòng ban",
        field: "department",
        emptyValue: "Chưa có thông tin"
      }
    ],
    data: []
  });
  const [selectEmp, setSelectEmp] = useState(null);
  useEffect(() => {
    dispatch(getList());
  }, []);

  useEffect(() => {
    if (prop.max != undefined){
      setState({ columns: [
        { title: "ID", field: "idEmp" },
        { title: "Tên", field: "fullname" },
        { title: "Điện thoại", field: "phone", emptyValue: "Chưa có thông tin" },
        { title: "Email", field: "email", emptyValue: "Chưa có thông tin" },
        {
          title: "Phòng ban",
          field: "department",
          emptyValue: "Chưa có thông tin"
        }
      ], data: listEmployes.slice(0, parseInt(prop.max, 10)) });
    } else
    setState({ ...state, data: listEmployes });
    // console.log(listEmployes);
  }, [listEmployes]);

  const isDelete = useSelector(state => state.employes.deleteEmploy);
  useEffect(() => {
    if (isDelete.length !== 0) {
      toast.toastSuccess(`Xóa nhân viên thành công`);
      dispatch(resetEmloy());
      dispatch(getList());
    }
  }, [isDelete]);
  const [open, setOpen] = React.useState(false);

  // Edit success
  const isEditEmploy = useSelector(state => state.employes.editEmploy);
  useEffect(() => {
    if (isEditEmploy) {
      toast.toastSuccess("Chỉnh sửa thông tin thành công");
      dispatch(hide());
      dispatch(getListNor());
      // dispatch(getInfor())
      dispatch(resetEmloy());
    }
  }, [isEditEmploy]);

  return (
    <Fragment>
      <MaterialTable
        title="Danh sách nhân viên"
        columns={state.columns}
        data={state.data}
        options={{
          pageSize: 10,
          exportFileName: "Danh sách nhân viên",
          exportAllData: true,
          exportButton: true
        }}
        localization={{
          toolbar:{
            exportName: 'Xuất file exel',
            exportTitle:'Xuất file exel',
          }
        }}
        onRowClick={(evt, selectedRow) => {
          setSelectEmp(selectedRow);
          setOpen(true);
          setTimeout(() => {
            setOpen(false);
          }, 200);
        }}
        editable={{
          onRowDelete: oldData =>
            new Promise(resolve => {
              resolve();
              setState(prevState => {
                dispatch(delEmploye(oldData.id));
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            })
        }}
      />
      <Dialog
        title="Thông tin nhân viên"
        isOpen={open}
        onSubmit={() => {
          dispatch(onEditEmloy());
        }}
      >
        <EditEmploye employes={selectEmp} removeTitle />
      </Dialog>
    </Fragment>
  );
}

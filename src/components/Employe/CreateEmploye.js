/* eslint-disable eqeqeq */
/* eslint-disable react-hooks/exhaustive-deps */
import DateFnsUtils from "@date-io/date-fns";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { getListUnset } from "actions/accountActions";
import { resetError } from "actions/authActions";
import { createNewEmp } from "actions/benefitActions";
import { getListContracts } from "actions/contractActions";
import { getListDepart } from "actions/departmentActions";
import { addNewEmp, getList, resetEmloy } from "actions/employeActions";
import { getByDepart } from "actions/positonAction";
import { hide } from "actions/spinerActions";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import GridContainer from "components/Grid/GridContainer.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import "date-fns";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import * as toast from "utils/toasHelper";

const useStyles = makeStyles(theme => ({
  formControl: {
    width: "100%",
    margin: "27px 0 0 0",
    paddingBottom: "10px",
    minWidth: 120
  },
  cardCategory: {
    textTransform: "uppercase"
  },
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  },
  description: {
    color: "#972FB0",
    fontWeight: "400"
  }
}));

export default function CreateEmploye({ ...rest }) {
  const isEmpty = require("is-empty");
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [fullname, setFullname] = useState("");
  const [account, setAccount] = useState("");
  const [address, setAddress] = useState("");
  const [phone, setPhone] = useState("");
  const [idCard, setIdCard] = useState("");
  const [dateOfIssue, setDateOfIssue] = useState(new Date("2010-01-02"));
  const [placeOfIssue, setPlaceOfIssue] = useState("");
  const [religion, setReligion] = useState("");
  const [nationlaty, setnationlaty] = useState("");
  const [idEmp, setIdEmp] = useState("");
  const [note, setNote] = useState("");
  const [department, setDepartment] = useState("");
  const [contract, setContract] = useState("");
  const [position, setPosition] = useState("");
  const [birthplace, setBirthplace] = useState("");
  const [birthdate, setBirthdate] = useState(new Date("2000-01-02"));
  const [gender, setGender] = useState(1);
  const dispatch = useDispatch();
  const [depPartChange, setDepPartChange] = useState(false);

  const checkRole = () => {
    return ["suAd", "ad", 'adNs', 'nvNs'].includes(localStorage.getItem('role'));
  };

  // Create error
  const hasErr = useSelector(state => state.errors);
  useEffect(() => {
    if (!isEmpty(hasErr)) {
      Swal.fire({
        icon: "error",
        title: Object.keys(hasErr)[0],
        text: Object.values(hasErr)[0]
      });
      dispatch(resetError());
      dispatch(hide());
    }
  }, [hasErr]);

  // Create success
  const isCreateSuccess = useSelector(state => state.employes.createEmploye);
  useEffect(() => {
    if (isCreateSuccess) {
      toast.toastSuccess("Thêm nhân viên thành công");
      dispatch(hide());
      dispatch(getList());
      dispatch(resetEmloy());
      dispatch(createNewEmp({
        year: new Date().getFullYear().toString(),
        month: (new Date().getMonth() + 1).toString(),
        idEmp
      }));
      resetForm();
    }
  }, [isCreateSuccess]);

  // get Data
  const handleChangeDepart = e => {
    setDepPartChange(true);
    setDepartment(e.target.value);
    dispatch(getByDepart(e.nativeEvent.target.getAttribute("idDpmt")));
  };
  const resetForm = () => {
    setAddress("");
    setFullname("");
    setEmail("");
    setAccount("");
    setPlaceOfIssue("");
    setPhone("");
    setReligion("");
    setGender("");
    setIdEmp("");
    setBirthdate(new Date("2000-01-02"));
    setDateOfIssue(new Date("2010-01-02"));
    setBirthplace("");
    setIdCard("");
    setDepartment("")
    setPosition("")
    setnationlaty("");
    setNote("");
    setContract("");
  }
  const listAccUnAssign = useSelector(state => state.accounts.listAccUnset);

  useEffect(() => {
    if (checkRole()) {
      dispatch(getListDepart());
      dispatch(getListUnset());
      dispatch(getListContracts());
    }

    return () => {
      dispatch(resetEmloy());
    };
  }, []);

  const listCons = useSelector(state => state.cons.listCons);
  const rendertListCons = () => {
    return listCons.map(item => {
      return (
        <MenuItem key={item.id} value={item.idContr}>
          {item.note}
        </MenuItem>
      );
    });
  };
  const listDeparts = useSelector(state => state.departs.list);
  const rendertListDepart = () => {
    return listDeparts.map(item => {
      return (
        <MenuItem key={item.id} value={item.name} idDpmt={item.idDpmt}>
          {item.name}
        </MenuItem>
      );
    });
  };

  const listPos = useSelector(state => state.positions.listPosFilter);
  const rendertListPos = () => {
    return listPos.map(item => {
      return (
        <MenuItem key={item.id} value={item.name}>
          {item.name}
        </MenuItem>
      );
    });
  };

  const rendertListAccUnset = () => {
    return listAccUnAssign.map((item, index) => {
      return (
        <MenuItem key={index} value={item}>
          {item}
        </MenuItem>
      );
    });
  };

  //On submit form
  const onChangeProfile = e => {
    if (!isEmpty(idEmp)) {
      dispatch(
        addNewEmp({
          gender,
          email,
          account,
          fullname,
          address,
          note,
          phone,
          dateOfIssue,
          placeOfIssue,
          birthplace,
          position,
          birthdate,
          idCard,
          contract,
          nationlaty,
          religion,
          idEmp,
          department
        })
      );
    }
  }

  return (
    <Card>
      {rest.removeTitle == undefined && (
        <CardHeader color="primary">
          <h4 className={classes.cardTitleWhite}>Thêm nhân viên</h4>
        </CardHeader>
      )}

      <CardBody>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <CustomInput
              labelText="Họ và tên"
              id="company-disabled"
              formControlProps={{
                fullWidth: true
              }}
              onChange={e => {
                setFullname(e.target.value);
              }}
              inputProps={{
                value: fullname
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            {isEmpty(rest.employes) ? (
              <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">Tài khoản</InputLabel>
                <Select
                  value={account}
                  onChange={e => {
                    setAccount(e.target.value);
                  }}
                >
                  {listAccUnAssign.length > 0 && rendertListAccUnset()}
                </Select>
              </FormControl>
            ) : (
                <CustomInput
                  labelText="Tài khoản"
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    disabled: true,
                    value: account
                  }}
                />
              )}
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <CustomInput
              labelText="Địa chỉ mail"
              formControlProps={{
                fullWidth: true
              }}
              onChange={e => {
                setEmail(e.target.value);
              }}
              inputProps={{
                value: email,
                disabled: rest.employes ? true : false
              }}
            />
          </GridItem>
        </GridContainer>

        <GridContainer>
          <GridItem xs={12} sm={12} md={3}>
            <CustomInput
              labelText="Mã nhân viên"
              onChange={e => {
                setIdEmp(e.target.value);
              }}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                value: idEmp,
                disabled: rest.employes ? true : false
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <FormControl className={classes.formControl}>
              <InputLabel>Phòng ban</InputLabel>
              <Select
                value={department}
                onChange={e => handleChangeDepart(e)}
              >
                {listDeparts.length > 0 && rendertListDepart()}
              </Select>
            </FormControl>
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            {depPartChange ? (
              <FormControl className={classes.formControl}>
                <InputLabel>Vị trí</InputLabel>
                <Select
                  value={position}
                  onChange={e => {
                    setPosition(e.target.value);
                  }}
                >
                  {listPos.length > 0 && rendertListPos()}
                </Select>
              </FormControl>
            ) : (
                <CustomInput
                  labelText="Vị trí"
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    value: position,
                    disabled: true
                  }}
                />
              )}
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <FormControl className={classes.formControl}>
              <InputLabel>Hợp đồng</InputLabel>
              <Select
                value={contract}
                onChange={e => {
                  setContract(e.target.value);
                }}
              >
                {listCons.length > 0 && rendertListCons()}
              </Select>
            </FormControl>
          </GridItem>
        </GridContainer>

        <GridContainer>
          <GridItem xs={12} sm={12} md={3}>
            <CustomInput
              labelText="CMND/ Passport"
              onChange={e => {
                setIdCard(e.target.value);
              }}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                value: idCard
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={6}>
            <CustomInput
              labelText="Nơi cấp"
              onChange={e => {
                setPlaceOfIssue(e.target.value);
              }}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                value: placeOfIssue
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                className={classes.formControl}
                margin="normal"
                id="date-picker-dialog"
                label="Ngày cấp"
                format="MM/dd/yyyy"
                value={dateOfIssue}
                onChange={val => {
                  setDateOfIssue(val);
                }}
                KeyboardButtonProps={{
                  "aria-label": "change date"
                }}
              />
            </MuiPickersUtilsProvider>
          </GridItem>
        </GridContainer>

        <GridContainer>
          <GridItem xs={12} sm={12} md={3}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                className={classes.formControl}
                margin="normal"
                id="date-picker-dialog"
                label="Ngày sinh"
                format="MM/dd/yyyy"
                value={birthdate}
                onChange={val => {
                  setBirthdate(val);
                }}
                KeyboardButtonProps={{
                  "aria-label": "change date"
                }}
              />
            </MuiPickersUtilsProvider>
          </GridItem>

          <GridItem xs={12} sm={12} md={6}>
            <CustomInput
              labelText="Quê quán"
              onChange={e => {
                setBirthplace(e.target.value);
              }}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                value: birthplace
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={3}>
            <FormControl className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">Giới tính</InputLabel>
              <Select
                value={gender}
                onChange={e => {
                  setGender(e.target.value);
                }}
              >
                <MenuItem value={0}>Nam</MenuItem>
                <MenuItem value={1}>Nữ</MenuItem>
                <MenuItem value={2}>Khác</MenuItem>
              </Select>
            </FormControl>
          </GridItem>
        </GridContainer>

        <GridContainer>
          <GridItem xs={12} sm={12} md={3}>
            <CustomInput
              labelText="Số điện thoại"
              onChange={e => {
                setPhone(e.target.value);
              }}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                value: phone
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={5}>
            <CustomInput
              labelText="Địa chỉ"
              onChange={e => {
                setAddress(e.target.value);
              }}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                value: address
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={2}>
            <CustomInput
              labelText="Quốc tịch"
              onChange={e => {
                setnationlaty(e.target.value);
              }}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                value: nationlaty
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={2}>
            <CustomInput
              labelText="Tôn giáo"
              onChange={e => {
                setReligion(e.target.value);
              }}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                value: religion
              }}
            />
          </GridItem>
        </GridContainer>

        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <InputLabel style={{ color: "#AAAAAA" }}>Ghi chú</InputLabel>
            <CustomInput
              labelText="Để lại dòng vài dòng miêu tả về bản thân"
              id="about-me"
              onChange={e => {
                setNote(e.target.value);
              }}
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                multiline: true,
                rows: 5,
                value: note
              }}
            />
          </GridItem>
        </GridContainer>
      </CardBody>
      {rest.removeTitle == undefined && (
        <CardFooter>
          <Button color="primary" onClick={onChangeProfile} disabled={idEmp === ""}>
            Thêm nhân viên
          </Button>
        </CardFooter>
      )}
    </Card>
  );
}

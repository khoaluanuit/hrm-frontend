/* eslint-disable react-hooks/exhaustive-deps */
import { makeStyles } from "@material-ui/core/styles";
import SelectCom from "react-select";
import { useDispatch, useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import { getListNor, resetEmloy, showDialog } from "actions/employeActions";

const useStyles = makeStyles(theme => ({
  formControlSearch: {
    width: 500,
    display: "inline-block",
    zIndex: 10,
    color: "black"
  }
}));

export default function SearchInforEmp({ ...props }) {
  const classes = useStyles();
  const isEmpty = require("is-empty");
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getListNor());
    return () => {
      dispatch(resetEmloy());
    };
  }, []);

  const [inputSearch, setInputSearch] = useState("");
  const listEmployes = useSelector(state => state.employes.listEmploye);
  useEffect(() => {
    if (!isEmpty(listEmployes)) {
      listEmployes.forEach(element => {
        element.value = element.idEmp;
        element.label = `${element.fullname} - ${element.idEmp}`;
      });
    }
  }, [listEmployes]);
  return (
    <SelectCom
      className={classes.formControlSearch}
      options={listEmployes}
      menuIsOpen={inputSearch !== ""}
      defaultValue={props.data}
      maxMenuHeight={200}
      onChange={e => {
        dispatch(showDialog({ title: "Thông tin nhân viên", data: e, type: 'search' }));
      }}
      placeholder="Tìm kiếm thông tin nhân viên"
      onInputChange={e => {
        setInputSearch(e);
      }}
    />
  );
}

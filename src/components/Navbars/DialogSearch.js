/* eslint-disable no-useless-escape */
/* eslint-disable react-hooks/exhaustive-deps */
import AppBar from "@material-ui/core/AppBar";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Slide from "@material-ui/core/Slide";
import { makeStyles } from "@material-ui/core/styles";
import Switch from "@material-ui/core/Switch";
import TextField from "@material-ui/core/TextField";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import BeachAccessIcon from "@material-ui/icons/BeachAccess";
import BeenhereIcon from "@material-ui/icons/Beenhere";
import CloseIcon from "@material-ui/icons/Close";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import ImageIcon from "@material-ui/icons/Image";
import LockIcon from "@material-ui/icons/Lock";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import WorkIcon from "@material-ui/icons/Work";
import { changePassword, resetDelAcc } from "actions/accountActions";
import { logoutUser, resetError } from "actions/authActions";
import { resetDialog } from "actions/employeActions";
import avatar from "assets/img/faces/avatar.png";
import Card from "components/Card/Card.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import React, { useEffect } from "react";
import PasswordStrengthBar from "react-password-strength-bar";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import SearchInforEmp from "./SearchInforEmp";

const useStyles = makeStyles(theme => ({
  avatarSty: {
    width: 60,
    height: 60,
    margin: "auto",
    backgroundColor: theme.palette.secondary.main
  },
  appBar: {
    position: "relative"
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  list: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  wrapper: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 100
  },
  card: {
    width: "unset",
    minWidth: 350,
    marginRight: 30,
    border: "1px solid gray",
    boxShadow: "0 1px 4px 0 rgba(0, 0, 0, 0.14)"
  },
  floatR: {
    position: "absolute",
    right: 20
  },
  description: {
    color: "#972FB0",
    fontWeight: "400"
  },
  cardCategory: {
    textTransform: "uppercase"
  },
  cardPass: {
    display: "flex",
    alignItems: "center"
  },
  warning: {
    maxWidth: 450,
    margin: "auto",
    listStyleType: "none"
  }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function DialogSearch({ ...props }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const isEmpty = require("is-empty");
  const [open, setOpen] = React.useState(false);
  const [title, setTitle] = React.useState(null);
  const [type, setType] = React.useState(null);
  const [data, setData] = React.useState({});
  const dialogFull = useSelector(state => state.employes.dialogFull);

  //Password
  const [pass, setPass] = React.useState("");
  const [passNew, setPassNew] = React.useState("");
  const [passNewCon, setPassNewCon] = React.useState("");
  const checkPassword = () => {
    if (
      passNewCon === passNew &&
      passNew.length > 8 &&
      /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/.test(passNew)
    ) {
      return true;
    }
    return false;
  };
  useEffect(() => {
    if (!isEmpty(dialogFull)) {
      setOpen(true);
      setTitle(dialogFull.title);
      !isEmpty(dialogFull.data) && setData(dialogFull.data);
      setType(dialogFull.type);
      dispatch(resetDialog());
    }
  }, [dialogFull]);
  useEffect(() => {
    return () => {
      dispatch(resetDelAcc());
    };
  }, []);

  const checkType = () => {
    return type === "search";
  };

  const handleClose = () => {
    setOpen(false);
  };
  const hasErr = useSelector(state => state.errors);
  //Popup Error
  useEffect(() => {
    if (Object.keys(hasErr).length !== 0 && hasErr.constructor === Object) {
      Swal.fire({
        icon: "error",
        title: Object.keys(hasErr)[0],
        text: Object.values(hasErr)[0]
      });
      dispatch(resetError());
    }
  }, [hasErr]);

  const changePassSuccess = useSelector(
    state => state.accounts.changePassSuccess
  );
  useEffect(() => {
    if (changePassSuccess) {
      Swal.fire({
        icon: "success",
        title: "Mật khẩu đã được thay đổi"
      }).then(() => {
        dispatch(logoutUser())
      });
    }
  }, [changePassSuccess]);

  const onChangePassword = () => {
    dispatch(
      changePassword({
        passwordOld: pass,
        password: passNew,
        password2: passNewCon
      })
    );
  };
  const ViewPassword = type === "password" && (
    <div>
      <Card className={classes.cardPass}>
        <div className={classes.paper}>
          <Avatar className={classes.avatarSty}>
            <LockOutlinedIcon style={{ height: "1.5em", width: "1.5em" }} />
          </Avatar>
          <TextField
            onChange={e => {
              setPass(e.target.value);
            }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Mật khẩu hiện tại"
            type="password"
          />

          <TextField
            onChange={e => {
              setPassNew(e.target.value);
            }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Mật khẩu mới"
            type="password"
          />
          <PasswordStrengthBar password={passNew} />
          <TextField
            onChange={e => {
              setPassNewCon(e.target.value);
            }}
            variant="outlined"
            margin="normal"
            fullWidth
            label="Nhập lại mật khẩu mới"
            type="password"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={!checkPassword()}
            onClick={onChangePassword}
          >
            Lưu thay đổi
          </Button>
        </div>
      </Card>
      <div className={classes.warning}>
        <ListItem>
          <ListItemIcon>
            <BeenhereIcon />
          </ListItemIcon>
          <ListItemText
            id="switch-list-label-wifi"
            primary="Độ dài mật khẩu mới phải lớn hơn 8 ký tự"
          />
          <ListItemSecondaryAction>
            <Switch
              edge="end"
              checked={passNew.length > 8}
              inputProps={{ "aria-labelledby": "switch-list-label-wifi" }}
            />
          </ListItemSecondaryAction>
        </ListItem>

        <ListItem>
          <ListItemIcon>
            <LockIcon />
          </ListItemIcon>
          <ListItemText
            id="switch-list-label-bluetooth"
            primary="Có chứa ít nhất một ký tự đặc biệt"
          />
          <ListItemSecondaryAction>
            <Switch
              edge="end"
              checked={/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/.test(passNew)}
              inputProps={{ "aria-labelledby": "switch-list-label-bluetooth" }}
            />
          </ListItemSecondaryAction>
        </ListItem>

        <ListItem>
          <ListItemIcon>
            <FileCopyIcon />
          </ListItemIcon>
          <ListItemText
            id="switch-list-label-bluetooth1"
            primary="Mật khẩu nhập lại phải trùng mật khẩu mới"
          />
          <ListItemSecondaryAction>
            <Switch
              edge="end"
              checked={passNew !== "" && passNew === passNewCon}
              inputProps={{ "aria-labelledby": "switch-list-label-bluetooth1" }}
            />
          </ListItemSecondaryAction>
        </ListItem>
      </div>
    </div>
  );
  const ViewSearch = type === "search" && (
    <div className={classes.wrapper}>
      <Card profile className={classes.card}>
        <CardAvatar profile>
          <a href="#pablo" onClick={e => e.preventDefault()}>
            <img src={avatar} alt="..." />
          </a>
        </CardAvatar>
        <CardBody profile>
          <div className={classes.cardCategory}>{data.position}</div>
          <div className={classes.cardCategory}>{data.department}</div>
          <h3 className={classes.cardTitle}>{data.fullname}</h3>
          <h2 className={classes.description}>{data.idEmp}</h2>
        </CardBody>
      </Card>
      <List className={classes.list}>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <ImageIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Số điện thoại"
            secondary={isEmpty(data.phone) ? "Chưa cập nhật" : data.phone}
          />
        </ListItem>

        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <ImageIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Email"
            secondary={isEmpty(data.email) ? "Chưa cập nhật" : data.email}
          />
        </ListItem>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <WorkIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Địa chỉ"
            secondary={isEmpty(data.address) ? "Chưa cập nhật" : data.address}
          />
        </ListItem>

        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <BeachAccessIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Ghi chú"
            secondary={isEmpty(data.note) ? "Chưa cập nhật" : data.note}
          />
        </ListItem>
      </List>
    </div>
  );
  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6">{title}</Typography>
            {checkType() && (
              <div className={classes.title}>
                <SearchInforEmp data={data} />
              </div>
            )}
            <Button
              className={classes.floatR}
              autoFocus
              color="inherit"
              onClick={handleClose}
            >
              Đóng
            </Button>
          </Toolbar>
        </AppBar>
        {ViewSearch}
        {ViewPassword}
      </Dialog>
    </div>
  );
}

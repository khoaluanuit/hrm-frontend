/* eslint-disable react-hooks/exhaustive-deps */
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import GroupOutlined from "@material-ui/icons/GroupOutlined";
import { resetError } from "actions/authActions";
import { getListDepart } from "actions/departmentActions";
import { addNewPos, getListPos, resetPos } from "actions/positonAction";
import background from "assets/img/register/background.jpg";
import Card from "components/Card/Card.js";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import * as toast from "utils/toasHelper";

const useStyles = makeStyles(theme => ({
  card: {
    marginTop: "0 !important"
  },
  formControl: {
    width: "100%",
    margin: "27px 0 0 0",
    paddingBottom: "10px",
    minWidth: 120
  },
  root: {
    height: "100vh"
  },
  image: {
    backgroundImage: `url(${background})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center"
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export default function CreatePosition({ ...rest }) {
  const classes = useStyles();
  //namtt
  const [name, setName] = useState("");
  const [idPosi, setIdPosi] = useState("");
  const [idDpmt, setIdDpmt] = useState("");
  const dispatch = useDispatch();

  const hasErr = useSelector(state => state.errors);
  const isAddPosSuccess = useSelector(state => state.positions.addPos);

  //Get list Depart
  const listDeparts = useSelector(state => state.departs.list);
  useEffect(() => {
    dispatch(getListDepart());
  }, []);

  const rendertListDeparts = () => {
    // console.log(listDeparts);
    return listDeparts.map(item => {
      return (
        <MenuItem key={item.id} value={item.idDpmt}>
          {item.name}
        </MenuItem>
      );
    });
  };

  //Popup Error
  useEffect(() => {
    if (Object.keys(hasErr).length !== 0 && hasErr.constructor === Object) {
      Swal.fire({
        icon: "error",
        title: Object.keys(hasErr)[0],
        text: Object.values(hasErr)[0]
      });
      dispatch(resetError());
    }
  }, [hasErr]);

  // Popup Success
  useEffect(() => {
    if (isAddPosSuccess) {
      toast.toastSuccess("Thêm vị trí thành công");
      dispatch(getListPos());
      dispatch(resetPos());
    }
  }, [isAddPosSuccess]);

  const onSubmit = e => {
    e.preventDefault();
    console.log({ name, idDpmt, idPosi });
    dispatch(addNewPos({ name, idDpmt, idPosi }));
  };

  return (
    <Card className={classes.card}>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <GroupOutlined />
        </Avatar>
        <Typography component="h1" variant="h5">
          Thêm mới vị trí
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            onChange={e => {
              setName(e.target.value);
            }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            placeholder="Trưởng phòng kế toán"
            label="Tên vị trí"
            autoFocus
          />
          <TextField
            onChange={e => {
              setIdPosi(e.target.value);
            }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            placeholder="TP-Ketoan"
            label="Mã vị trí"
            autoFocus
          />

          <FormControl className={classes.formControl}>
            <InputLabel>Phòng ban</InputLabel>
            <Select
              value={idDpmt}
              onChange={e => {
                setIdDpmt(e.target.value);
              }}
            >
              {listDeparts.length > 0 && rendertListDeparts()}
            </Select>
          </FormControl>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={e => {
              onSubmit(e);
            }}
          >
            Thêm
          </Button>
        </form>
      </div>
    </Card>
  );
}

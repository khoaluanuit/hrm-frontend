/* eslint-disable react-hooks/exhaustive-deps */
import { getListPos, delPos, resetPos, editPos } from "actions/positonAction";
import MaterialTable from "material-table";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as toast from "utils/toasHelper";

export default function ListPosition() {
  const isEmpty = require("is-empty");
  const listPos = useSelector(state => state.positions.listPos);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    columns: [
      { title: "Tên phòng", field: "nameDpmt", defaultGroupOrder: 0 },
      { title: "Mã vị trí", field: "idPosi" },
      { title: "Tên vị trí", field: "name" },
      { title: "Ngày tạo", field: "dateCreated", editable: "never" }
    ],
    data: []
  });

  useEffect(() => {
    dispatch(getListPos());
  }, []);

  useEffect(() => {
    listPos.forEach(item => {
      item.dateCreated = new Date(item.dateCreated).toLocaleDateString("vi-VN");
    });
    setState({ ...state, data: listPos });
  }, [listPos]);
  // Delete Pos
  const deletePos = useSelector(state => state.positions.deletePos);
  useEffect(() => {
    if (deletePos.length !== 0) {
      toast.toastSuccess(`Xóa vị trí thành công`);
      dispatch(resetPos());
    }
  }, [deletePos]);

  // Edit Pos
  const isEditSucces = useSelector(state => state.positions.editPos);
  useEffect(() => {
    if (!isEmpty(isEditSucces)) {
      toast.toastSuccess(`Chỉnh sửa vị trí thành công`);
      dispatch(resetPos());
    }
  }, [isEditSucces]);
  return (
    <MaterialTable
      title="Danh sách vị trí"
      columns={state.columns}
      options={{ pageSize: 10 }}
      data={state.data}
      onRowClick={(e, data) => {
        console.log(data);
      }}
      editable={{
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              setState(prevState => {
                if (
                  oldData.name !== newData.name ||
                  oldData.idPosi !== newData.idPosi
                ) {
                  dispatch(editPos(newData));
                }
                const data = [...prevState.data];
                const index = data.indexOf(oldData);
                data[index] = newData;
                return { ...prevState, data };
              });
              resolve();
            }, 500);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            resolve();
            setState(prevState => {
              dispatch(delPos(oldData.id));
              const data = [...prevState.data];
              data.splice(data.indexOf(oldData), 1);
              return { ...prevState, data };
            });
          })
      }}
    />
  );
}

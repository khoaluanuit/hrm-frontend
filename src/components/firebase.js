import app from 'firebase/app';
import "firebase/auth";
import "firebase/firestore";
import Swal from "sweetalert2";
const config = {
  apiKey: "AIzaSyAnTVb2n_i5DRGtAvtnh5kp2WVKa3hNfBI",
  authDomain: "hrmuit-cc10d.firebaseapp.com",
  databaseURL: "https://hrmuit-cc10d.firebaseio.com",
  projectId: "hrmuit-cc10d",
  storageBucket: "hrmuit-cc10d.appspot.com",
  messagingSenderId: "494451183828",
  appId: "1:494451183828:web:45ce28d1d3257fe8e82b41",
  measurementId: "G-EPKWHG6Z0X"
};

class Firebase {
  constructor() {
    app.initializeApp(config)
    app.auth().languageCode = 'vi'
    this.auth = app.auth();
    this.db = app.firestore();
    this.recaptchaVerifier = null;
    this.confirmationResult = null;
    this.captcha = ""
  }

  renderCaptcha(cb) {
    setTimeout(() => {
      this.recaptchaVerifier = new app.auth.RecaptchaVerifier('recaptcha-container', {
        'callback': (response) => {
          this.captcha = response;
          cb();
        }
      });
      this.recaptchaVerifier.render();
    }, 500);
  }

  renderCaptchaMail( cb) {
    setTimeout(() => {
      this.recaptchaVerifier = new app.auth.RecaptchaVerifier('recaptcha-reset-mail', {
        'callback': (response) => {
          this.captcha = response;
          cb();
        }
      });
      this.recaptchaVerifier.render();
    }, 500);
  }

  verifyCaptcha(cb) {
    this.recaptchaVerifier.verify().then(res => {
      console.log(res);
      cb()
    }).catch(err => {
      console.log(err);
    })
  }

  confirmOTP(otp, cb) {
    this.confirmationResult.confirm(otp).then((res) => {
      // console.log(res);
      cb(res)
    }).catch(err => {
      cb(err)
    })
  }

  phoneAuth(emp) {

    if (emp.phone > 0) {
      emp.phone = `+84${emp.phone}`;

      this.auth.signInWithPhoneNumber(emp.phone, this.recaptchaVerifier).then((otp) => {
        this.confirmationResult = otp;
        console.log('Da gui OTP');
      }).catch(function (error) {
        Swal.fire({
          icon: "error",
          title: 'Too many request',
          text: error.message
        });
      })
    } else {
      Swal.fire({
        icon: "error",
        title: 'Không tìm thấy sdt',
        text: 'Vui lòng liên hệ admin để kiểm tra'
      });
    }

    // this.auth.signInWithPhoneNumber(emp.phone, this.recaptchaVerifier).then(async (otp) => {
    //     isCheck = true
    //     this.confirmationResult = otp;
    //     console.log(otp);
    //     // alert(otp);
    //     const { value: text } = await Swal.fire({
    //         title: 'Xác thực OTP',
    //         input: 'number',
    //         inputPlaceholder: 'Mã OTP đã được gửi về điện thoại của bạn'
    //     })

    //     if (text) {
    //         Swal.fire(`Entered text: ${text}`)
    //     }
    // }).catch(function (error) {
    //     console.log('error');
    //     Swal.fire({
    //         icon: "error",
    //         title: 'Too many request',
    //         text: error.message
    //     });

    // })

  }

}

export default new Firebase()
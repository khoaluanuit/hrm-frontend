import React from 'react';

const SpinnerIcon = props => {
  return (
    <svg width="80px" height="80px"
      xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" className="lds-double-ring" style={{background: "none"}}>
      <circle cx="50" cy="50" fill="none" strokeDashoffset="round" r="40" strokeWidth="4" stroke="#1d3f72" strokeDasharray="62.83185307179586 62.83185307179586" transform="rotate(175.425 50 50)">
        <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1.3s" begin="0s" repeatCount="indefinite"></animateTransform>
      </circle>
      <circle cx="50" cy="50" fill="none" strokeDashoffset="round" r="35" strokeWidth="4" stroke="#5699d2" strokeDasharray="54.97787143782138 54.97787143782138"  transform="rotate(-175.425 50 50)">
        <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;-360 50 50" keyTimes="0;1" dur="1.3s" begin="0s" repeatCount="indefinite"></animateTransform>
      </circle>
    </svg>
  )
}
export default SpinnerIcon;
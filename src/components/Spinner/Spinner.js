import React from 'react';
import SpinerIcon from './Icon';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from "react-redux";

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
  },
  spinner: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: ' translate(-50%, -50%)',
    zIndex: '1000'
  }

}));

const Spinner = props => {
  const classes = useStyles();
  const isVisible = useSelector(state => state.loading)

  return (
    <div className={classes.spinner}>
      {isVisible ? <SpinerIcon /> : null}
    </div>
  )
}
export default Spinner;
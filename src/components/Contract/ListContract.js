/* eslint-disable react-hooks/exhaustive-deps */
import { delCons, getListContracts, resetCons } from "actions/contractActions";
import MaterialTable from "material-table";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as toast from "utils/toasHelper";

export default function ListContracts() {
  const listCons = useSelector(state => state.cons.listCons);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    columns: [
      { title: "Mã hợp đồng", field: "idContr" },
      { title: "Giờ làm", field: "workingH" },
      { title: "Lương cơ bản", field: "baseSala" },
      { title: "Trợ cấp", field: "allowance" },
      { title: "Bảo hiểm", field: "insurance" },
      {title: "Ghi chú", field: "note" }
    ],
    data: []
  });

  useEffect(() => {
    dispatch(getListContracts());
  }, []);

  useEffect(() => {
      console.log(listCons)
    setState({ ...state, data: listCons });
  }, [listCons]);

  const deleteCons = useSelector(state => state.cons.deleteCons);
  useEffect(() => {
    if (deleteCons.length !== 0) {
      toast.toastSuccess(`Xóa hợp đồng thành công`);
      dispatch(resetCons());
    }
  }, [deleteCons]);

  return (
    <MaterialTable
      title="Danh sách hợp đồng"
      columns={state.columns}
      options={{ pageSize: 8 }}
      data={state.data}
      onRowClick={(e, data) => {
        console.log(data);
      }}
      editable={{
        onRowDelete: oldData =>
          new Promise(resolve => {
            resolve();
            setState(prevState => {
              dispatch(delCons(oldData.id));
              const data = [...prevState.data];
              data.splice(data.indexOf(oldData), 1);
              return { ...prevState, data };
            });
          })
      }}
    />
  );
}

/* eslint-disable react-hooks/exhaustive-deps */
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import EventNoteOutlined from "@material-ui/icons/EventNoteOutlined";
import { resetError } from "actions/authActions";
import { addNewCons, getListContracts, resetCons } from "actions/contractActions";
import Card from "components/Card/Card.js";
import React, { useEffect, useState } from "react";
import NumberFormat from "react-number-format";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import * as toast from "utils/toasHelper";


const useStyles = makeStyles(theme => ({
  card: {
    marginTop: "0 !important"
  },
  formControl: {
    width: "100%",
    margin: "27px 0 0 0",
    paddingBottom: "10px",
    minWidth: 120
  },
  root: {
    height: "100vh"
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));
function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            value: values.value
          }
        });
      }}
      thousandSeparator
      isNumericString
      suffix=" đ"
    />
  );
}
export default function CreateContract({ ...rest }) {
  const classes = useStyles();
  //namtt
  //const [name, setName] = useState("");
  const [type, setType] = useState("");
  const [insurance, setInsurance] = useState("");
  const [idContr, setidContr] = useState("");
  const [note, setNote] = useState("");
  const [workingH, setWorkingH] = useState("");
  const [baseSala, setBaseSala] = useState("");
  const [allowance, setAllowance] = useState("");

  const dispatch = useDispatch();

  
  const isAddConsSuccess = useSelector(state => state.cons.addCons);
  const hasErr = useSelector(state => state.errors);
  //Popup Error
  useEffect(() => {
    if (Object.keys(hasErr).length !== 0 && hasErr.constructor === Object) {
      Swal.fire({
        icon: "error",
        title: Object.keys(hasErr)[0],
        text: Object.values(hasErr)[0]
      });
      dispatch(resetError());
    }
  }, [hasErr]);

  // Popup Success
  useEffect(() => {
    if (isAddConsSuccess) {
      toast.toastSuccess("Thêm hợp đồng thành công");
      dispatch(getListContracts());
      dispatch(resetCons());
    }
  }, [isAddConsSuccess]);

  const onSubmit = e => {
    e.preventDefault();
    console.log({
      type,
      idContr,
      insurance,
      note,
      workingH,
      baseSala,
      allowance,
      startDate: "1/11/2019",
      endDate: "1/11/2020",
      payDate: "5",
      payForms: "Thẻ tín dụng/ ATM"
    });
    dispatch(
      addNewCons({
        type,
        idContr,
        insurance,
        note,
        workingH,
        baseSala,
        allowance,
        startDate: "1/11/2019",
        endDate: "1/11/2020",
        payDate: "5",
        payForms: "Thẻ tín dụng/ ATM"
      })
    );
  };

  return (
    <Card className={classes.card}>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <EventNoteOutlined />
        </Avatar>
        <Typography component="h1" variant="h5">
          Thêm mới hợp đồng
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            onChange={e => {
              setidContr(e.target.value);
            }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            placeholder="TP-HD-KT-2"
            label="Mã hợp đồng"
          />
          <TextField
            onChange={e => {
              setNote(e.target.value);
            }}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            placeholder="Hợp đồng cho nhân viên kế toán bậc 2"
            label="Ghi chú"
          />
          <TextField
            onChange={e => {
              setBaseSala(e.target.value);
            }}
            variant="outlined"
            margin="normal"
            fullWidth
            placeholder="1,000,000 đ"
            label="Lương cơ bản"
            InputProps={{
              inputComponent: NumberFormatCustom
            }}
          />
          <TextField
            onChange={e => {
              setAllowance(e.target.value);
            }}
            variant="outlined"
            margin="normal"
            fullWidth
            placeholder="1,000,000 đ"
            label="Trợ cấp"
            InputProps={{
              inputComponent: NumberFormatCustom
            }}
          />

          <FormControl className={classes.formControl}>
            <InputLabel>Loại hợp đồng</InputLabel>
            <Select
              value={type}
              onChange={e => {
                setType(e.target.value);
              }}
            >
              <MenuItem value="Hợp đồng lao động">Hợp đồng lao động</MenuItem>
              <MenuItem value="Hợp đồng thời vụ">Hợp đồng thời vụ</MenuItem>
            </Select>
          </FormControl>
          <FormControl className={classes.formControl}>
            <InputLabel>Gói bảo hiểm</InputLabel>
            <Select
              value={insurance}
              onChange={e => {
                setInsurance(e.target.value);
              }}
            >
              <MenuItem value="BHXH, BHYT, BHTN (1.500.000đ)">
                BHXH, BHYT, BHTN (1.500.000đ)
              </MenuItem>
              <MenuItem value="BHXH, BHYT, BHTN (3.500.000đ)">
                BHXH, BHYT, BHTN (3.500.000đ)
              </MenuItem>
              <MenuItem value="BHXH, BHYT, BHTN (6.500.000đ)">
                BHXH, BHYT, BHTN (6.500.000đ)
              </MenuItem>
            </Select>
          </FormControl>
          <FormControl className={classes.formControl}>
            <InputLabel>Thời gian làm việc</InputLabel>
            <Select
              value={workingH}
              onChange={e => {
                setWorkingH(e.target.value);
              }}
            >
              <MenuItem value="Làm theo ca 8h - 12h">
                Làm theo ca 8h - 12h, có thứ 7
              </MenuItem>
              <MenuItem value="Ngày làm việc 8h (Sáng: 8h-12h, Chiều 1h-5h), thứ 2 - thứ 6">
                Ngày làm việc 8h (Sáng: 8h-12h, Chiều 1h-5h), thứ 2 - thứ 6
              </MenuItem>
              <MenuItem value="Ngày làm việc 12h (Sáng: 8h-12h, Chiều 1h- 7h), thứ 2 - thứ 6">
                Ngày làm việc 12h (Sáng: 8h-12h, Chiều 1h- 7h), thứ 2 - thứ 6
              </MenuItem>
            </Select>
          </FormControl>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={e => {
              onSubmit(e);
            }}
          >
            Thêm
          </Button>
        </form>
      </div>
    </Card>
  );
}
 
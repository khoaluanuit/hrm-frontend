/* eslint-disable react-hooks/exhaustive-deps */
import { deleteDepart, edittDepart, getListDepart, resetDelDepart } from "actions/departmentActions";
import { hide } from 'actions/spinerActions';
import MaterialTable from 'material-table';
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as toast from "utils/toasHelper";

export default function ListDepart() {
  const listDeparts = useSelector((state) => state.departs.list);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    columns: [
      { title: 'Mã phòng ban', field: 'idDpmt', editable: "never" },
      { title: 'Tên phòng ban', field: 'name' },
      { title: 'Email', field: 'email' },
      { title: 'Người quản lý', field: 'manager' },
    ],
    data: [],
  });

  useEffect(() => {
    dispatch(getListDepart());
  }, []);

  useEffect(() => {
    setState({ ...state, data: listDeparts })
    dispatch(hide());
  }, [listDeparts]);

  const isDelete = useSelector((state) => state.departs.deleteDepart);
  useEffect(() => {
    if (isDelete.length !== 0) {
      toast.toastSuccess(`Xóa phòng ban thành công`);
      dispatch(resetDelDepart());
    }
  }, [isDelete]);

  return (
    <MaterialTable
      title="Danh sách phòng ban"
      columns={state.columns}
      data={state.data}
      editable={{
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              setState(prevState => {

                if (
                  oldData.name !== newData.name ||
                  oldData.manager !== newData.manager ||
                  oldData.email !== newData.email
                ) {
                  console.log(oldData);
                  console.log(newData);
                  dispatch(edittDepart(newData));
                }
                const data = [...prevState.data];
                const index = data.indexOf(oldData);
                data[index] = newData;
                return { ...prevState, data };
              });
              resolve();
            }, 500);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            resolve();
            setState(prevState => {
              dispatch(deleteDepart(oldData._id))
              const data = [...prevState.data];
              data.splice(data.indexOf(oldData), 1);
              return { ...prevState, data };
            });
          }),
      }}
    />
  );
}

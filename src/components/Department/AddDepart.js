/* eslint-disable react-hooks/exhaustive-deps */
// @material-ui/core components
import FormControl from '@material-ui/core/FormControl';
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { makeStyles } from "@material-ui/core/styles";
import { resetError } from "actions/authActions";
import { addNewDepart, getListDepart } from "actions/departmentActions";
import { getList } from "actions/employeActions";
import { show } from 'actions/spinerActions';
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import GridContainer from "components/Grid/GridContainer.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import * as toast from "utils/toasHelper";

const useStyles = makeStyles(theme => ({
  formControl: {
    width: '100%',
    margin: '27px 0 0 0',
    paddingBottom: '10px',
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
}));

export default function AddDepart() {

  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [name, setname] = useState("");
  const [idDpmt, setidDpmt] = useState("");
  const [manager, setManager] = useState("");

  const dispatch = useDispatch();
  // handle Error
  const hasErr = useSelector((state) => state.errors);
  useEffect(() => {
    if (Object.keys(hasErr).length !== 0 && hasErr.constructor === Object) {
      Swal.fire({
        icon: "error",
        title: Object.keys(hasErr)[0],
        text: Object.values(hasErr)[0]
      });
      dispatch(resetError());
    }

  }, [hasErr]);

  // handle Add success
  const isCreateSuccess = useSelector((state) => state.departs.createDepart);
  useEffect(() => {
    if (isCreateSuccess.hasOwnProperty('name')) {
      toast.toastSuccess(`Thêm ${isCreateSuccess.name} thành công`);
      dispatch(show());
      dispatch(getListDepart());
    }
  }, [isCreateSuccess]);

  //On submit form
  const onsubmit = (e) => {
    dispatch(addNewDepart({
      email, idDpmt, manager, name,
    }))
  }

  const onChangeManager = event => {
    console.log(event.target.value);
    setManager(event.target.value);
  }
  // getList
  useEffect(() => {
    dispatch(getList())
  }, []);

  const listEmployes = useSelector((state) => state.employes.listEmploye);
  const rendertListEmpl = () => {
    return listEmployes.map((item) => {
      return <MenuItem key={item.idEmp} value={item.fullname}>{item.fullname}</MenuItem>
    })
  }
  return (
    <Card>
      <CardHeader color="primary">
        <h4 className={classes.cardTitleWhite}>Thêm phòng ban</h4>
      </CardHeader>

      <CardBody>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <CustomInput
              labelText="Mã phòng ban"
              formControlProps={{
                fullWidth: true
              }}
              onChange={(e) => { setidDpmt(e.target.value) }}
            />
            <CustomInput
              labelText="Tên phòng ban"
              formControlProps={{
                fullWidth: true
              }}
              onChange={(e) => { setname(e.target.value) }}
            />
            <FormControl className={classes.formControl} >
              <InputLabel id="demo-simple-select-label">Người quản Lý</InputLabel>
              <Select
                value={manager}
                onChange={onChangeManager}
              >
                {listEmployes.length > 0 && rendertListEmpl()}
              </Select>
            </FormControl>
            <CustomInput
              labelText="Email"
              formControlProps={{
                fullWidth: true
              }}
              onChange={(e) => { setEmail(e.target.value) }}
            />
          </GridItem>
        </GridContainer>
      </CardBody>

      <CardFooter>
        <Button color="primary" onClick={onsubmit}>Thêm</Button>
      </CardFooter>
    </Card>
  );
}

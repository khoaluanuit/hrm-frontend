import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useSelector  } from "react-redux";


export default ({ component: Component, ...rest }) => {
  const auth = useSelector(state=>state.auth)
  return (
    <Route
      {...rest}
      render={({ props,location }) => {
        return   auth.isAuthenticated ? (
          <Component {...props}></Component>
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: location }
            }}
          />
        )
      }

      }
    />
  );
};
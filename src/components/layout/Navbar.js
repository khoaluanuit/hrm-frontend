import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";

class Navbar extends Component {
    onLogoutClick = e => {
        e.preventDefault();
        this.props.logoutUser();
    };
    render() {
        if (!this.props.auth.isAuthenticated) {
            return (
                <div className="navbar-fixed">
                    <nav className="z-depth-10">
                        <div className="nav-wrapper pink">
                            <div className="mailcontact">
                                <i className="material-icons mailicon">email</i>
                                <span>linhtrinh@pavietnam.vn</span>
                            </div>
                            <div className="phonecontact"></div>
                            <i className="material-icons phoneicon">phone</i>
                            0327773869
                            <div />
                        </div>
                    </nav>
                </div>
            );
        } else {
            const { user } = this.props.auth;
            return (
                <div>
                    <div className="navbar-fixed">
                        <nav className="z-depth-10">
                            <div className="nav-wrapper pink">
                                <div className="dropdown">
                                    <a href="/" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" className = "material-icons mailico usericon">
                                        person
                                    </a>
                                    <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a className="dropdown-item" href="/" onClick={this.onLogoutClick}>Logout</a>
                                        <a className="dropdown-item" href={"/your-infomation/" + user.id}>Thông tin nhân viên</a>
                                        <a className="dropdown-item" href="/listusers">Something else here</a>
                                    </div>
                                </div>
                                <p>{user.name}</p>
                            </div>
                        </nav>
                    </div>

                </div>
            );
        }
    }
}

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(
    mapStateToProps,
    { logoutUser }
)(withRouter(Navbar));
import React from "react";
import { Route, Redirect } from "react-router-dom";
import Auth from "./Auth";
export const PrivateRoute = ({ component: Component, ...rest }) => {

  
  return (
    <Route
      {...rest}
      render={({ props,location }) => {
        console.log(rest) 
        console.log(Auth) 
        console.log(props) ;
        return Auth.authenticated ? (
          <Component {...props}></Component>
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: location }
            }}
          />
        )
      }

      }
    />
  );
};

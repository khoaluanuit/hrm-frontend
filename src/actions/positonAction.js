import { addPos, delById, editById, getList, getPosByDepart, getPosByNameDepart } from "../apis/position";
import * as account from "../constants/account";
import * as pos from "../constants/position";
export const getListPos = () => dispatch => {
  getList()
    .then(res => {
      dispatch({
        type: pos.GET_LIST_POS,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const addNewPos = data => dispatch => {
  addPos(data)
    .then(res => {
      dispatch({
        type: pos.ADD_POSITION,
        payload: res.data
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const editPos = data => dispatch => {
  editById(data)
    .then(res => {
      dispatch({
        type: pos.EDIT_POSITION,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getByNameDep = data => dispatch => {
  getPosByNameDepart({ name: data })
    .then(res => {
      dispatch({
        type: pos.GET_LIST_POSFIL,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getByDepart = data => dispatch => {
  getPosByDepart({ idDpmt: data })
    .then(res => {
      dispatch({
        type: pos.GET_LIST_POSFIL,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};
export const delPos = data => dispatch => {
  delById(data)
    .then(res => {
      dispatch({
        type: pos.DELETE_POSITION,
        payload: res.data.deletePosition
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};
export const resetPos = () => dispatch => {
  dispatch({
    type: pos.RESET_POSITION
  });
};

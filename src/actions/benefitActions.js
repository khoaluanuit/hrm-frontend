import * as benefit from "../constants/benefit";
import { createById,getYourMonsById, createMonth, getYourKeep, getKeepById } from "../apis/benefits";
import * as account from "../constants/account";

export const createNewEmp = (data) => dispatch => {
  createById(data)
    .then(res => {
      dispatch({
        type: benefit.CREATE_NEW_TIME,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const createAllEmp = () => dispatch => {
  createMonth()
    .then(res => {
      console.log(res.data);
      dispatch({
        type: benefit.CREATE_NEW_ALL,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};
export const getListMonths = () => dispatch => {
  getYourMonsById()
    .then(res => {
      dispatch({
        type: benefit.GET_LIST_MONTHS,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getKeepIdEmp = (data) => dispatch => {
  getKeepById(data)
    .then(res => {
      dispatch({
        type: benefit.GET_YOUR_BEN,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};
export const getYourTimeKeep = data => dispatch => {
  // console.log(data);
  getYourKeep(data)
    .then(res => {
      dispatch({
        type: benefit.GET_YOUR_BEN,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const resetYourBens = () => dispatch => {
  dispatch({
    type: benefit.RESET_YOUR_BEN
  });
};

export const resetBens = () => dispatch => {
  dispatch({
    type: benefit.RESET_BENEFIT
  });
};

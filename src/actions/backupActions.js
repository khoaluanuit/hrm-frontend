import * as backup from "../constants/backup";
import { getAllBack, createBack, deleteBack, restoreBack } from "../apis/backup";
import * as account from "../constants/account";

export const createNewBackup = () => dispatch => {
    createBack()
        .then(res => {
            dispatch({
                type: backup.CREATE_BACKUP,
                payload: "Backup thành công"
            });
        })
        .catch(err => {
            dispatch({
                type: account.GET_ERRORS,
                payload: err.response.data
            });
        });
};

export const getListBackups = () => dispatch => {
    getAllBack()
        .then(res => {

            dispatch({
                type: backup.GET_LIST_BACKUP,
                payload: res.data
            });
        })
        .catch(err => {
            dispatch({
                type: account.GET_ERRORS,
                payload: err.response.data
            });
        });
};

export const delBackup = (id) => dispatch => {
    deleteBack(id)
        .then(res => {
            dispatch({
                type: backup.DELETE_BACKUP,
                payload: "Xóa thành công!"
            });
        })
        .catch(err => {
            dispatch({
                type: account.GET_ERRORS,
                payload: err.response.data
            });
        });
};

export const restoreBackup = (id) => dispatch => {
    restoreBack(id)
        .then(res => {
            dispatch({
                type: backup.DELETE_BACKUP,
                payload: "Phục hồi dữ liệu thành công!"
            });
        })
        .catch(err => {
            dispatch({
                type: account.GET_ERRORS,
                payload: err.response.data
            });
        });
};

export const resetBackup = () => dispatch => {
    dispatch({
        type: backup.RESET_BACKUP
    });
};

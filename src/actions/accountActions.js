// Update infor user
import { resetPassUser, verifyUser, resetPassAd, delAcc, editAcc, editPass, getListAcc, getListAccUnAssign, getListRoleFunc } from "../apis/accounts";
import * as account from "../constants/account";

export const changePassword = (data) => dispatch => {
  editPass(data)
    .then(res => {
      dispatch({
        type: account.CHANGE_PASS,
        payload: true
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getList = () => dispatch => {
  getListAcc()
    .then(res => {
      dispatch({
        type: account.GETLIST_ACC,
        payload: res.data
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const delAccount = id => dispatch => {
  delAcc(id)
    .then(res => {
      dispatch({
        type: account.DELETE_ACC,
        payload: res.data.deleteuser
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const resetDelAcc = id => dispatch => {
  dispatch({
    type: account.RESET_ACCOUNTS
  });
};

export const getListUnset = () => dispatch => {
  getListAccUnAssign()
    .then(res => {
      dispatch({
        type: account.GETLIST_UNSET,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const editList = data => dispatch => {
  // delete data["email"];
  console.log(data);
  editAcc(data)
    .then(res => {
      dispatch({
        type: account.EDIT_ACCOUNT,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getListRole = () => dispatch => {
  getListRoleFunc()
    .then(res => {
      dispatch({
        type: account.GET_LIST_ROLE_FUNC,
        payload: res.data.funcs
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const resetPassAdminAct = (id) => dispatch => {
  resetPassAd(id)
    .then(res => {
      dispatch({
        type: account.RESET_PASS_ADMIN,
        payload: res.data.Message
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const resetPassUserAct = (data) => dispatch => {
  // {
  //   lastOTP
  //   email
  // }
  resetPassUser(data)
    .then(res => {
      dispatch({
        type: account.RESET_PASS_USER,
        payload: res.data.Message
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const verifyUserAct = (data) => dispatch => {
  // {
  //   email
  // }
  console.log(data)
  verifyUser(data)
    .then(res => {
      dispatch({
        type: account.VERIFY_USER,
        payload: res.data.Message
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const addSession = () => dispatch => {
  dispatch({
    type: account.SET_SESSION
  });
};
// Update infor user
import { show } from "./spinerActions";
import * as account from "../constants/account";
import {
  getListEmp,
  getListNormalEmp,
  getYourInfor,
  addEmploye,
  delEmpl,
  editEmpl,
  editEmplNormal
} from "../apis/employes";

export const addNewEmp = userData => dispatch => {
  dispatch(show());
  addEmploye(userData)
    .then(res => {
      dispatch({
        type: account.CREATE_USER
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};
export const getInfor = () => dispatch => {
  getYourInfor()
    .then(res => {
      dispatch({
        type: account.GET_YOUR_EMPLOY,
        payload: res.data[0]
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getList = () => dispatch => {
  getListEmp()
    .then(res => {
      dispatch({
        type: account.GETLIST_SUCCESS,
        payload: res.data
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getListNor = () => dispatch => {
  getListNormalEmp()
    .then(res => {
      dispatch({
        type: account.GETLIST_SUCCESS,
        payload: res.data
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const delEmploye = id => dispatch => {
  delEmpl(id)
    .then(res => {
      dispatch({
        type: account.DEL_EMPLOYE,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const resetEmloy = () => dispatch => {
  dispatch({
    type: account.RESET_DEL_EMPLOYE
  });
};
export const onEditEmloy = (data) => dispatch => {
  dispatch({
    type: account.ON_EDIT_EMPLOYE
  });
};

export const editEmloy = userData => dispatch => {
  dispatch(show());
  editEmpl(userData)
    .then(res => {
      dispatch({
        type: account.EDIT_EMPLOYE,
        payload: true
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const editEmloyNor = userData => dispatch => {
  dispatch(show());
  editEmplNormal(userData)
    .then(res => {
      dispatch({
        type: account.EDIT_EMPLOYE,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
}; 

// Dialog
export const showDialog = userData => dispatch => {
  dispatch({
    type: account.SHOW_FULL_DIALOG,
    payload: userData
  });
};

export const showDialogMail = userData => dispatch => {
  dispatch({
    type: account.SHOW_FULL_DIALOG_MAIL,
    payload: userData
  });
};

export const resetDialog = userData => dispatch => {
  dispatch({
    type: account.RESET_DIALOG,
    payload: userData
  });
};
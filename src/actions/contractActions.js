import * as contract from "../constants/contract";
import { getList, delById, addPos, editById } from "../apis/contracts";
import * as account from "../constants/account";

export const getListContracts = () => dispatch => {
  getList()
    .then(res => {
      dispatch({
        type: contract.GET_LIST_CONTRACT,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const addNewCons = data => dispatch => {
  addPos(data)
    .then(res => {
      dispatch({
        type: contract.ADD_CONTRACT,
        payload: res.data
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const editCons = data => dispatch => {
  editById(data)
    .then(res => {
      dispatch({
        type: contract.EDIT_CONTRACT,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const delCons = data => dispatch => {
  console.log(data)
  delById(data)
    .then(res => {
      dispatch({
        type: contract.DELETE_CONTRACT,
        payload: res.data.deletePosition
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};
export const resetCons = () => dispatch => {
  dispatch({
    type: contract.RESET_CONTRACT
  });
};

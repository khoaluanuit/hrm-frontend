import axios from "axios";

import * as auth from "../constants/auth";
import * as spinner from "./spinerActions";
import * as userApis from "../apis/accounts";
import * as authApis from "../apis/auth";
// Register User
export const registerUser = (userData) => (dispatch) => {
  dispatch(spinner.show());
  userApis
    .register(userData)
    .then((res) =>
      dispatch({
        type: auth.REGISTER_SUCCESS
      })
    )
    .catch((err) => {
      console.log(err);
      dispatch({
        type: auth.GET_ERRORS,
        payload: err.response.data
      });
    });
};

// Login & Set token
export const loginUser = (userData) => (dispatch) => {
  dispatch(spinner.show());
  userApis
    .logInUser(userData)
    .then((res) => {
      dispatch({
        type: auth.LOGIN_SUCCESS,
        payload: res.data
      })
      // const { token } = res.data;
      // const decoded = jwt_decode(token);
      // localStorage.setItem("x-auth-token", token);
      // localStorage.setItem('account', decoded.email)
      // localStorage.setItem('role', decoded.role)
      // dispatch(setCurrentUser(decoded));
    })
    .catch((err) =>
      dispatch({
        type: auth.GET_ERRORS,
        payload: err.response.data
      })
    )
    .finally(() => {
      dispatch(spinner.hide());
    });
};

// Set logged in user
export const setCurrentUser = (decoded) => {
  return {
    type: auth.SET_CURRENT_USER,
    payload: decoded
  };
};

export const resetError = () => {
  return {
    type: auth.RESET_ERRORS
  };
};
export const resetRegister = () => {
  return {
    type: auth.RESET_REGISTER
  };
};
export const getUsers = () => (dispatch) => {
  axios.get("/users").then((res) =>
    dispatch({
      type: auth.GET_USERS,
      payload: res.data
    })
  );
};

export const getUser = (id) => (dispatch) => {
  axios.get(`/users/detail-user/${id}`).then((res) => {
    dispatch({
      type: auth.GET_USER,
      payload: res.data
    });
  });
};

export const logoutUser = () => (dispatch) => {
  authApis.logout().then((res) => {
    if (res.data) {
      console.log(res.data);
      localStorage.removeItem("x-auth-token");
      localStorage.removeItem("role");
      localStorage.removeItem("account");
      dispatch(setCurrentUser({}));
    }
  });
};

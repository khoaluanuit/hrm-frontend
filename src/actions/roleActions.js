import * as role from "../constants/role";
import { getList } from '../apis/roles';
import * as account from "../constants/account";

export const getListRole = () => dispatch => {
    getList()
    .then(res => {
      dispatch({
        type: role.GET_LIST_ROLE,
        payload: res.data
      })
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    }
    );
};


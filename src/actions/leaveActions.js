import { getAll, getAllYour, create,delLeave, approveLeave } from "../apis/leave";
import * as leave from "../constants/leave";
import * as account from "../constants/account";

export const createLeave = (data) => dispatch => {
  create(data)
    .then(res => {
      dispatch({
        type: leave.CREATE_LEAVE,
        payload: res.data.hasOwnProperty('idEmp')
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const editLeave = (data) => dispatch => {

  approveLeave(data)
    .then(res => {
      console.log(res.data)
      dispatch({
        type: leave.EDIT_LEAVE,
        payload: res.data.message
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const deleteLeave = (data) => dispatch => {

  delLeave(data)
    .then(res => {
      dispatch({
        type: leave.DELETE_LEAVE,
        payload: res.data.message
      });
    })
    .catch(err => { 
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getAllLeave = () => dispatch => {
  getAll()
    .then(res => {
      // console.log(res.data.leavesNotApproveYet)
      dispatch({
        type: leave.GET_ALL_LEAVENOTYET,
        payload: res.data.leavesNotApproveYet
      });
    })
    .catch(err => {
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getAllYourLeave = () => dispatch => {
  getAllYour()
    .then(res => { 
      dispatch({
        type: leave.GET_YOUR_LEAVE,
        payload: res.data.leavesAll
      });
    })
    .catch(err => {
      console.log(err); 
      dispatch({
        type: account.GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const resetLeave = () => dispatch => {
  dispatch({
    type: leave.RESET_LEAVE
  });
};

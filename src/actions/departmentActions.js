// Update infor user
import * as depart from "../constants/department";
import { getList, addDepart, deleteDept,editById } from '../apis/departments';

export const edittDepart = (data) => dispatch => {
    console.log(data);
    editById(data)
        .then(res => {
            console.log(res.data);
            dispatch({
                type: depart.GET_LIST,
                payload: res.data
            })
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: depart.GET_LIST,
                payload: err.response
            });
        }
        );
};

export const getListDepart = () => dispatch => {
    getList()
        .then(res => {
            dispatch({
                type: depart.GET_LIST,
                payload: res.data
            })
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: depart.GET_LIST,
                payload: err.response
            });
        }
        );
};

export const addNewDepart = (data) => dispatch => {
    addDepart(data)
        .then(res => {
            dispatch({
                type: depart.ADD_DEPART,
                payload: res.data
            })
        })
        .catch(err => {
            console.log(err);
              dispatch({
                type: 'GET_ERRORS',
                payload: err.response.data
              });
        }
        );
};

export const deleteDepart = (id) => dispatch => {
    deleteDept(id)
        .then(res => {
            dispatch({
                type: depart.DELETE_DEPART,
                payload: res.data.deleteDepartment
            })
        })
        .catch(err => {
            dispatch({
                type: depart.GET_LIST,
                payload: err.response
            });
        }
        );
};

export const resetDelDepart= () => dispatch => {
    dispatch({
        type: depart.RESET_DELETE
    })
};
import * as spin from "../constants/spinner";

export const show = () => {
  return {
    type: spin.DATA_LOADING
  };
};

export const hide = () => {
  return {
    type: spin.DATA_DONE
  };
};

// export const showDialog = () => {
//   return {
//     type: spin.SHOW_DIALOG
//   };
// };

// export const hideDialog = () => {
//   return {
//     type: spin.HIDE_DIALOG
//   };
// };
